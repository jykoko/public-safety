# Software Development II -- Public Safety Application #

This repository is for the Stetson iOS team's contribution for the public safety application. We decided to create the iPhone version, were all but one of us had experience. Through much research, pair-programming, and teamwork we were able to compile a working iPhone app.

# About

The iPhone application works in sync with a website front end that Stetson University's Public Safety would use. If a student feels unsafe or is in need of help they could press a button which would notify Public Safety of that student's location. 