//
//  PSSchool.m
//  PublicSafety
//
//  Created by Antonio Garcia on 10/26/14.
//  Copyright (c) 2014 we.engineer.net.jykoko. All rights reserved.
//

#import "PSSchool.h"
#import "AFNetworking.h"
#import "PSConstants.h"

@interface PSSchool()
    //private instances go here...
@end

@implementation PSSchool
@synthesize schoolEmail = _schoolEmail;
@synthesize schoolId = _schoolId;
@synthesize schoolPhoneNumb = _schoolPhoneNumb;
@synthesize schoolAddress = _schoolAddress;


/*
 * Get particular school based on schoolId from request handler
 * I created completion block handlers to copy Parse because we are familiar with Parse
 */
-(void)getSchool:(NSNumber*)schoolId
{
    [PSRequestHandler getSchoolFromJSON:schoolId andUrlString:PSSchoolApiBaseUrl andCompletion:^(NSDictionary *jsonResponse) {
        if(jsonResponse != nil)
        {
            self.schoolId=jsonResponse[@"ID"];
            self.schoolEmail=jsonResponse[@"EmailDomain"];
            self.schoolAddress=jsonResponse[@"Address"];
            self.schoolPhoneNumb=jsonResponse[@"PhoneNumber"];
            NSLog(@"school ID: %@", self.schoolId);
            NSLog(@"school Id: %@", self.schoolEmail);
            NSLog(@"school Address: %@", self.schoolAddress);
            NSLog(@"school Phone Numb: %@", self.schoolPhoneNumb);
        }
    }];
}

/*
 * Get list of all schools from request handler
 * I created completion block handlers to copy Parse because we are familiar with Parse
 */
-(void)getAllSchools
{
    [PSRequestHandler getAllSchoolsFromJSON:PSSchoolApiBaseUrl andCompletion:^(NSDictionary *jsonResponse) {
        if(jsonResponse != nil)
        {
            self.schoolId=jsonResponse[@"ID"];
            self.schoolEmail=jsonResponse[@"EmailDomain"];
            self.schoolAddress=jsonResponse[@"Address"];
            self.schoolPhoneNumb=jsonResponse[@"PhoneNumber"];
            NSLog(@"school ID: %@", self.schoolId);
            NSLog(@"school Id: %@", self.schoolEmail);
            NSLog(@"school Address: %@", self.schoolAddress);
            NSLog(@"school Phone Numb: %@", self.schoolPhoneNumb);
        }
    }];
}



@end