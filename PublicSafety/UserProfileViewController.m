//
//  UIViewController+UserProfileViewController.m
//  PublicSafety
//
//  Created by Jacob Koko on 10/28/14.
//  Copyright (c) 2014 we.engineer.net.jykoko. All rights reserved.
//
#import "UserProfileViewController.h"
#import "PSUser.h"
#import "PSRequestHandler.h"
#import "PSConstants.h"
#import "PSUser.h"
#import "ViewController.h"

@implementation UserProfileViewController
{
    PSUser* user;
    NSArray* stats;
    MBProgressHUD* progHud;
}

@synthesize userNameLabel = _userNameLabel;
@synthesize userOrganizationIdentifierLable = _userOrganizationIdentifierLabel;
@synthesize userProfileImageView = _userProfileImageView;
@synthesize userStatsTableView = _userStatsTableView;
@synthesize currentUser;

BOOL newMedia;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    newMedia = NO;
    
    self.userStatsTableView.delegate = self;
    self.userStatsTableView.dataSource = self;
    
    progHud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:progHud];
    progHud.delegate = self;
    [progHud show:YES];
    progHud.labelText = @"Loading Profile...";
    
    self.currentUser = [[PSUser alloc]init];
    
    //round image corners, and set default image
    self.userProfileImageView.layer.cornerRadius =  self.userProfileImageView.frame.size.height/2;
    self.userProfileImageView.layer.masksToBounds = YES;
    self.userProfileImageView.layer.borderWidth = 5;
    [self.userProfileImageView.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    self.userProfileImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.userProfileImageView.image = [UIImage imageNamed:@"imgres.jpg"];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style: UIBarButtonItemStyleBordered target:self action:@selector(Back)];
    self.navigationItem.leftBarButtonItem = backButton;
    self.navigationController.navigationBar.tintColor = [UIColor redColor];
    
    UIBarButtonItem* cameraBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCamera target:self action:@selector(addPhoto)];
    self.navigationItem.rightBarButtonItem = cameraBarButtonItem;
    [cameraBarButtonItem setTintColor:[UIColor blackColor]];
    
    [self populateUserDisplayData];
}


/*
 * This data will not be pulled here. Instead, it will be pulled when user logs in. However, since
 * authentication is not complete, this method is used for now.
 */
- (void)populateUserDisplayData
{
    self.userNameLabel.text=[NSString stringWithFormat:@"%@,%@", self.currentUser.userLastName, self.currentUser.userFirstName];
    
    stats = @[[NSString stringWithFormat:@"Username: %@",self.currentUser.userUserName],[NSString stringWithFormat:@"Email:%@",self.currentUser.userOrganizationIdentifierLable], [NSString stringWithFormat:@"School: %@", self.currentUser.userSchool]];
    
    self.userProfileImageView.image = self.currentUser.userImage;
    
    [progHud hide:YES];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHidden:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma UITableView Delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([stats count] > 0)
    {
         return [stats count];
    }
    else
    {
        return 0;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"AlertTypeCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell  alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    cell.textLabel.text = [stats objectAtIndex:indexPath.row];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    //UITableViewCell* cell = (UITableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
}

- (IBAction)Back
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)addPhoto
{
    NSString *other1 = @"Take Picture";
    NSString *other2 = @"Camera Roll";
    NSString *cancelTitle = @"Cancel";
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:nil
                                  delegate:self
                                  cancelButtonTitle:cancelTitle
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:other1, other2, nil];
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]) {
            UIImagePickerController *imagePicker =
            [[UIImagePickerController alloc] init];
            imagePicker.delegate = self;
            imagePicker.sourceType =
            UIImagePickerControllerSourceTypeCamera;
            imagePicker.mediaTypes = [NSArray arrayWithObjects:
                                      (NSString *) kUTTypeImage,
                                      nil];
            imagePicker.allowsEditing = NO;
            [self presentViewController:imagePicker animated:YES completion:nil];
            newMedia = YES;
        }
    }
    else if (buttonIndex == 1)
    {
        if ([UIImagePickerController isSourceTypeAvailable:
             UIImagePickerControllerSourceTypeSavedPhotosAlbum]) {
            
            UIImagePickerController *imagePicker =
            [[UIImagePickerController alloc] init];
            imagePicker.delegate = self;
            imagePicker.sourceType =
            UIImagePickerControllerSourceTypePhotoLibrary;
            imagePicker.mediaTypes = [NSArray arrayWithObjects:
                                      (NSString *) kUTTypeImage,
                                      nil];
            imagePicker.allowsEditing = NO;
            [self presentViewController:imagePicker animated:YES completion:^{}];
            newMedia = NO;
        }
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage])
    {
        [picker dismissViewControllerAnimated:YES completion:nil];
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        
        
        if (newMedia)
        {
            UIImageWriteToSavedPhotosAlbum(image,
                                           self,
                                           @selector(image:finishedSavingWithError:contextInfo:),nil);
        }
        
        UIImage* scaledAndOrientedImage = [self scaleImage:image toSize:CGSizeMake(200, 200)];
        NSData *imageData = UIImageJPEGRepresentation(scaledAndOrientedImage,0.8);
        self.userProfileImageView.image = [UIImage imageWithData:imageData];
    }
    else if ([mediaType isEqualToString:(NSString *)kUTTypeMovie])
    {
        // Code here to support video if enabled
    }
}

#pragma Utility Image Methods
/*
 * Put these methods in seperate photo utility class!
 */
- (UIImage*)scaleImage:(UIImage*)image toSize:(CGSize)newSize {
    CGSize scaledSize = newSize;
    float scaleFactor = 1.0;
    if( image.size.width > image.size.height ) {
        scaleFactor = image.size.width / image.size.height;
        scaledSize.width = newSize.width;
        scaledSize.height = newSize.height / scaleFactor;
    }
    else {
        scaleFactor = image.size.height / image.size.width;
        scaledSize.height = newSize.height;
        scaledSize.width = newSize.width / scaleFactor;
    }
    
    UIGraphicsBeginImageContextWithOptions( scaledSize, NO, 0.0 );
    CGRect scaledImageRect = CGRectMake( 0.0, 0.0, scaledSize.width, scaledSize.height );
    [image drawInRect:scaledImageRect];
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return scaledImage;
}

- (void)image:(UIImage *)image
finishedSavingWithError:(NSError *)error
  contextInfo:(void *)contextInfo {
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Save failed"
                              message: @"Failed to save image"
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
}

- (IBAction)logOut:(id)sender
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:NO forKey:@"LoggedIn"];
    [defaults synchronize];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    ViewController *loginVc = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self presentViewController:loginVc animated:YES completion:nil];
}
@end