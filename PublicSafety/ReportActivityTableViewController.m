//
//  UITableViewController+ReportActivityTableViewController.m
//  PublicSafety
//
//  Created by Jacob Koko on 11/13/14.
//  Copyright (c) 2014 we.engineer.net.jykoko. All rights reserved.
//

#import "ReportActivityTableViewController.h"
#import "PSRequestHandler.h"
#import "PSConstants.h"
#import "PSLocManagerSingleton.h"
#import "PSCountDownAlertView.h"
#import "MBProgressHUD.h"
#import "CustomAlertTypeCell.h"


@implementation ReportActivityTableViewController
{
    NSArray* reportTypes;
    NSArray* reportTypeId;
    NSNumber* alertID;
}

PSLocManagerSingleton* locationManager;
CLLocation* currentLoc;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
     self.navigationItem.title = @"Report Activity";
    
    //Adds a backbutton
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style: UIBarButtonItemStyleBordered target:self action:@selector(Back)];
    //Adds the custom red color to the back button
    [backButton setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor redColor]} forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = backButton;
    
    MBProgressHUD* HUD =[[MBProgressHUD alloc]init];
    HUD.labelText = @"Loading alerts...";
    [HUD show:YES];
    [self.navigationController.view addSubview:HUD];
    
    reportTypes = [[NSMutableArray alloc]init];
   [PSRequestHandler getAlerts:^(NSDictionary *dictionary) {
        if(dictionary)
        {
            NSLog(@"dict: %@",dictionary );
            
            reportTypes = [dictionary allValues];
            reportTypeId = [dictionary allKeys];
            
            for(int i = 0; i < [reportTypeId count]; i++)
            {
                NSLog(@"id: %@", [reportTypeId objectAtIndex:i]);
            }
            
            [self.tableView reloadData];
            [HUD hide:YES];
        }
   }];
}


#pragma UITableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section)
    {
        case 0:
            return [reportTypes count];
            break;
        default:
            return 0;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"AlertTypeCell";
    CustomAlertTypeCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        cell = [[CustomAlertTypeCell  alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    switch (indexPath.section)
    {
        {
        case 0:
            cell.alertTypeLabel.text = [reportTypes objectAtIndex:indexPath.row];
            NSString* alertTypeLbl = [reportTypes objectAtIndex:indexPath.row];
            cell.alertTypeImgView.image = [self returnAlertTypeImage:alertTypeLbl];
            [cell.checkMarkImageView setHidden:YES];
            break; }
        {
        default:
            cell.alertTypeLabel.text = @"Not Found";
            cell.alertTypeImgView.image = [self returnAlertTypeImage:@""];
            break;
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    CustomAlertTypeCell* cell = (CustomAlertTypeCell*)[tableView cellForRowAtIndexPath:indexPath];
    
    if([cell.checkMarkImageView isHidden])
    {
        [cell.checkMarkImageView setHidden:NO];
    } else {
        [cell.checkMarkImageView setHidden:YES];
    }
    
    alertID = [NSNumber numberWithLong: [[reportTypeId objectAtIndex:indexPath.row]longValue]];
    
    UIAlertView* checkResponse = [[UIAlertView alloc]initWithTitle:@"Send Report?" message:@"Are you sure you want to send this report?" delegate:self cancelButtonTitle:@"Submit" otherButtonTitles:@"Cancel", nil];
    [checkResponse show];
    
    
    NSLog(@"clicked id: %@", [reportTypeId objectAtIndex:indexPath.row]);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 51;
}

- (UIImage *)returnAlertTypeImage:(NSString *)alertType
{
    UIImage* alertTypeImg = NULL;
    [alertType lowercaseString];
    
    if ([alertType isEqualToString:@"Active Shooter"])
    {
        alertTypeImg = [UIImage imageNamed:@"active_shooter.png"];
    }
    else
    {
        alertTypeImg = [UIImage imageNamed:@"default_shield.png"];
    }
    return alertTypeImg;
}

- (IBAction)Back
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if((int)buttonIndex == 0)
    {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString* dateString = [dateFormatter stringFromDate:[NSDate date]];
        
        if( [[PSLocManagerSingleton sharedSingleton].userLocations count] > 0)
        {
            [PSRequestHandler addNewReport:dateString andLocations:[PSLocManagerSingleton sharedSingleton].userLocations andAlertID:alertID andCompletion:^(NSDictionary *dictionary) {
                if(dictionary)
                {
                    //display custom count down view
                    PSCountDownAlertView *alert = [[PSCountDownAlertView alloc] initWithFrame:CGRectMake(5, 100, 300, 200)];
                    [self.view addSubview:alert];
                    [alert show];
                }
            }];
        }
        else
        {
            [PSRequestHandler addNewReport:dateString andLocations:NULL andAlertID:alertID andCompletion:^(NSDictionary *dictionary) {
                if(dictionary)
                {
                    //display custom count down view
                    PSCountDownAlertView *alert = [[PSCountDownAlertView alloc] initWithFrame:CGRectMake(5, 100, 300, 200)];
                    [self.view addSubview:alert];
                    [alert show];
                }
            }];
        }
    }
}

@end