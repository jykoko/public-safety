//
//  UIViewController+RegistrationStepTwoViewController.m
//  PublicSafety
//
//  Created by Jacob Koko on 11/15/14.
//  Copyright (c) 2014 we.engineer.net.jykoko. All rights reserved.
//

#import "RegistrationStepTwoViewController.h"
#import "NSString+NSStringValidator.h"
#import "RegistrationStepOneViewController.h"

@implementation RegistrationStepTwoViewController

@synthesize userOrgIdField,registerButton,userConfirmationPinField;

-(void)viewDidLoad
{
    
    [super viewDidLoad];
    
    //self.registerButton.titleLabel.text = @"Register";
    [self.registerButton setTitle:@"Next" forState:UIControlStateNormal];
    
    
    
    //this should be org id? the email of the school or business?
    self.userOrgIdField.placeholder = @"Enter Organization Email";
    
    
    self.navigationController.navigationBar.tintColor = [UIColor redColor];
    
    
    self.userOrgIdField.delegate = self;
}

- (IBAction)registerNewUser:(id)sender
{
    BOOL isValid = [NSString validateEmail:userOrgIdField.text];
    if(isValid)
    {
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        RegistrationStepOneViewController *otherAlertsVc = [storyboard instantiateViewControllerWithIdentifier:@"RegistrationStepOneViewController"];
        otherAlertsVc.pin = userOrgIdField.text;
        [self.navigationController pushViewController:otherAlertsVc animated:YES];
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email Not Valid"
                                                        message:@"Please enter valid email"
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

#pragma mark PickerView DataSource

- (NSInteger)numberOfComponentsInPickerView:
(UIPickerView *)pickerView
{
    return 1;
}







@end
