//
//  NSObject+User.m
//  PublicSafety
//
//  Created by Jacob Koko on 10/16/14.
//  Copyright (c) 2014 we.engineer.net.jykoko. All rights reserved.
//

#import "PSUser.h"
#import "AFNetworking.h"
#import "PSConstants.h"

@interface PSUser()
  //private instances go here...
@end

@implementation PSUser

@synthesize userEmail = _userEmail;
@synthesize userFirstName = _userFirstName;
@synthesize userLastName = _userLastName;
@synthesize userImagePath = _userImagePath;
@synthesize userType = _userType;
@synthesize userId = _userId;
@synthesize userPhoneNumb = _userPhoneNumb;
@synthesize userLatitude = _userLatitude;
@synthesize userLongitude = _userLongitude;
@synthesize userImage = _userImage;
@synthesize userOrganizationIdentifierLable = _userOrganizationIdentifierLable;
@synthesize userRequestHandler = _userRequestHandler;
@synthesize userJsonResponse = _userJsonResponse;
@synthesize userUserName = _userUserName;
@synthesize userSchoolID = _userSchoolID;
@synthesize userSchool = _userSchool;
@synthesize userClientSecretToken = _userClientSecretToken;

-(id)init
{
    self = [super init];
    if(self)
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if(defaults)
        {
            NSData *data = [defaults objectForKey:@"userDetails"];
            NSDictionary *userDetailsDict = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            NSLog(@"user details: %@",userDetailsDict);
            
            //hard coded until authentication completed
            self.userImagePath = [[userDetailsDict valueForKey:@"Picture"]valueForKey:@"Data"];
            [self setProfileImage:self.userImagePath];
            
            self.userClientSecretToken = [userDetailsDict valueForKey:@"ClientSecretToken"];
            self.userId = [[userDetailsDict valueForKey:@"ID"]longValue];
            self.userFirstName = [userDetailsDict valueForKey:@"FName"];
            self.userLastName = [userDetailsDict valueForKey:@"LName"];
            self.userOrganizationIdentifierLable = [userDetailsDict valueForKey:@"OrganizationIdentifier"];
            self.userUserName = [userDetailsDict valueForKey:@"Username"];
            self.userSchool = [[userDetailsDict valueForKey:@"School"]valueForKey:@"Name"];
            self.userSchoolID  = [[[userDetailsDict valueForKey:@"School"]valueForKey:@"ID"]longValue];
        }
    }
    
    return self;
}

-(void)setProfileImage:(NSString*)pictureDataString
{
    NSString* base64EncodedImage = pictureDataString;
    
    if(base64EncodedImage != [NSNull null])
    {
        NSLog(@"data to decode: %@",base64EncodedImage);
        NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:base64EncodedImage options:0];
        
        //Now data is decoded. You can convert them to UIImage
        if(decodedData != nil)
        {
            UIImage *userImg = [UIImage imageWithData:decodedData];
            self.userImage = userImg;
        }
        else
        {
            self.userImage = [UIImage imageNamed:@"imgres.jpg"];
        }
    }
    else
    {
        self.userImage = [UIImage imageNamed:@"imgres.jpg"];
    }
}


@end