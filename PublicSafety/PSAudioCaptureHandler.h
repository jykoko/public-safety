//
//  NSObject+PSAudioCaptureHandler.h
//  PublicSafety
//
//  Created by Jacob Koko on 11/26/14.
//  Copyright (c) 2014 we.engineer.net.jykoko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@interface  PSAudioCaptureHandler : NSObject <AVAudioRecorderDelegate, AVAudioPlayerDelegate>
{
    
}

@property AVAudioRecorder *recorder;
@property AVAudioPlayer *player;

@end
