//
//  CustomSaleTypeCell.h
//  StoryBoard
//
//  Created by Jacob Koko
//
#import <UIKit/UIKit.h>

@interface CustomAlertTypeCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *alertTypeImgView;
@property (strong, nonatomic) IBOutlet UILabel *alertTypeLabel;
@property (strong, nonatomic) IBOutlet UIImageView *checkMarkImageView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *loadSpinner;

@end
