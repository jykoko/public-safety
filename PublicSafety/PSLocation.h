//
//  PSLocation.h
//  PublicSafety
//
//  Created by Connor Mashburn on 10/26/14.
//  Copyright (c) 2014 we.engineer.net.jykoko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface PSLocation : NSObject

@property(strong) NSNumber* locationID;
@property(strong) NSNumber* latitude;
@property(strong) NSNumber* longitude;
@property(strong) NSDate* date;

+(void)postLocationRequest;


@end