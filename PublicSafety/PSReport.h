//
//  PSReport.h
//  PublicSafety
//
//  Created by Admin on 10/26/14.
//  Copyright (c) 2014 we.engineer.net.jykoko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface PSReport : NSObject

@property(strong) NSNumber* reportID;
@property(strong) NSNumber* reportUserID;
@property(strong) NSNumber* reportAlertID;
@property(strong) NSNumber* reportStatusID;
@property(strong) NSNumber* reportLongitude;
@property(strong) NSNumber* reportLatitude;


@end
