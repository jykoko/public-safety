//
//  RegistrationStepOneViewController.m
//  PublicSafety
//
//  Created by Jacob Koko on 11/15/14.
//  Copyright (c) 2014 we.engineer.net.jykoko. All rights reserved.
//
#import "RegistrationStepOneViewController.h"
#import "MBProgressHUD.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "NSString+NSStringValidator.h"
#import "PSRequestHandler.h"
#import "PSConstants.h"

@implementation RegistrationStepOneViewController

@synthesize userEmailAddressField,userFullNameField,userPasswordAgainField,userPassWordField,userProfileImageView,userUsernameField,userCancellationPinField,registerBtn, pin;

BOOL newMedia;
int picId;

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    newMedia = NO;
    picId = -1;
    
    self.navigationItem.title = @"Registration - Step 2";
    
    [self.registerBtn setTitle:@"Register" forState:UIControlStateNormal];
    
    self.userUsernameField.delegate = self;
    self.userEmailAddressField.delegate = self;
    self.userFullNameField.delegate = self;
    self.userPasswordAgainField.delegate = self;
    self.userPassWordField.delegate = self;
    self.userCancellationPinField.delegate = self;
    
    self.userEmailAddressField.placeholder = @"Email Address";
    self.userFullNameField.placeholder = @"Full Name";
    self.userCancellationPinField.placeholder = @"4-Digit Security Pin";
    self.userUsernameField.placeholder = @"Enter Username";
    self.userPassWordField.placeholder = @"Enter Password";
    self.userPasswordAgainField.placeholder = @"Enter Password Again";
    
    self.userProfileImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.userProfileImageView.layer.cornerRadius = self.userProfileImageView.frame.size.height/2;
    self.userProfileImageView.layer.masksToBounds = YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

- (IBAction)uploadUserPhoto:(id)sender
{
    NSString *other1 = @"Take Picture";
    NSString *other2 = @"Camera Roll";
    NSString *cancelTitle = @"Cancel";
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:nil
                                  delegate:self
                                  cancelButtonTitle:cancelTitle
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:other1, other2, nil];
    [actionSheet showInView:self.view];
}

- (IBAction)registerUser:(id)sender
{
    MBProgressHUD* HUD =[[MBProgressHUD alloc]init];
    HUD.labelText = @"Registering new user...";
    [HUD show:YES];
    [self.view addSubview:HUD];
    
    BOOL isValid = [NSString passwordsMatch:userPassWordField.text andpasswordStrAgain:userPasswordAgainField.text];
    BOOL isValidAllowed = [NSString validatePassword:userPassWordField.text];
    NSString *passcode = [userCancellationPinField text];
    if(isValid)
    {
        if(isValidAllowed)
        {
            NSLog(@"Your Pass works");
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Password error"
                                                            message:@"Password has to be >8 and have a number"
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
            [alert show];
            [HUD hide:YES];
            return;
        }
    }
    else
    {
        NSLog(@"%i",isValid);
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Passwords error"
                                                        message:@"Your passwords do not match"
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
        [HUD hide:YES];
        return;
    }
    
    if(passcode.length == 4)
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        [defaults setObject:passcode forKey:@"securityPin"];
        [defaults synchronize];
        
        NSLog(@"Data saved");
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Pin Length"
                                                        message:@"Your pin must be four digits"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [HUD hide:YES];
        return;
    }
    //We create a new user
    
    if(![userFullNameField.text isEqualToString:@""])
    {
        NSArray* foo = [userFullNameField.text componentsSeparatedByString: @" "];
        NSString* firstName = [foo objectAtIndex: 0];
        NSString *lastName = [foo objectAtIndex:1];
        
        if(picId != -1)
        {
            [PSRequestHandler addNewUser:userFullNameField.text andPassword:userPassWordField.text andRoleId:[NSNumber numberWithInt:1] andFName:firstName andLName:lastName andSchoolID:[NSNumber numberWithInt:3] andPictureId:[NSNumber numberWithInt:picId] andOrgID:@"testing@stetson.edu" andCompletion:^(NSDictionary *jsonResponse) {
                if (jsonResponse)
                {
                    NSLog(@"success");
                    [HUD hide:YES];
                    [self dismissViewControllerAnimated:YES completion:nil];
                }
                else
                {
                    NSLog(@"failed");
                    [HUD hide:YES];
                    [self dismissViewControllerAnimated:YES completion:nil];
                }
            }];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Picture"
                                                            message:@"Please Select or take a picture"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            [HUD hide:YES];
            return;
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Name"
                                                        message:@"Please enter a full name"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [HUD hide:YES];
        return;
    }
    
 
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
        if (buttonIndex == 0)
        {
            if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]) {
                UIImagePickerController *imagePicker =
                [[UIImagePickerController alloc] init];
                imagePicker.delegate = self;
                imagePicker.sourceType =
                UIImagePickerControllerSourceTypeCamera;
                imagePicker.mediaTypes = [NSArray arrayWithObjects:
                                          (NSString *) kUTTypeImage,
                                          nil];
                imagePicker.allowsEditing = NO;
                [self presentViewController:imagePicker animated:YES completion:nil];
                newMedia = YES;
            }
        }
        else if (buttonIndex == 1)
        {
            if ([UIImagePickerController isSourceTypeAvailable:
                 UIImagePickerControllerSourceTypeSavedPhotosAlbum]) {
                
                UIImagePickerController *imagePicker =
                [[UIImagePickerController alloc] init];
                imagePicker.delegate = self;
                imagePicker.sourceType =
                UIImagePickerControllerSourceTypePhotoLibrary;
                imagePicker.mediaTypes = [NSArray arrayWithObjects:
                                          (NSString *) kUTTypeImage,
                                          nil];
                imagePicker.allowsEditing = NO;
                [self presentViewController:imagePicker animated:YES completion:^{}];
                newMedia = NO;
            }
        }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage])
    {
        [picker dismissViewControllerAnimated:YES completion:nil];
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        
        
        if (newMedia)
        {
            UIImageWriteToSavedPhotosAlbum(image,
                                           self,
                                           @selector(image:finishedSavingWithError:contextInfo:),nil);
        }
        
       UIImage* scaledAndOrientedImage = [self scaleImage:image toSize:CGSizeMake(200, 200)];
       NSData *imageData = UIImageJPEGRepresentation(scaledAndOrientedImage,0.8);
       self.userProfileImageView.image = [UIImage imageWithData:imageData];
        [PSRequestHandler addNewPicture:imageData andCompletion:^(NSDictionary *responseDict) {
            if(responseDict)
            {
                picId = [[responseDict objectForKey:@"ID"]intValue];
            }
        }];
    }
    else if ([mediaType isEqualToString:(NSString *)kUTTypeMovie])
    {
        // Code here to support video if enabled
    }
}

#pragma Utility Image Methods
/*
 * Put these methods in seperate photo utility class!
 */
- (UIImage*)scaleImage:(UIImage*)image toSize:(CGSize)newSize {
    CGSize scaledSize = newSize;
    float scaleFactor = 1.0;
    if( image.size.width > image.size.height ) {
        scaleFactor = image.size.width / image.size.height;
        scaledSize.width = newSize.width;
        scaledSize.height = newSize.height / scaleFactor;
    }
    else {
        scaleFactor = image.size.height / image.size.width;
        scaledSize.height = newSize.height;
        scaledSize.width = newSize.width / scaleFactor;
    }
    
    UIGraphicsBeginImageContextWithOptions( scaledSize, NO, 0.0 );
    CGRect scaledImageRect = CGRectMake( 0.0, 0.0, scaledSize.width, scaledSize.height );
    [image drawInRect:scaledImageRect];
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return scaledImage;
}

- (void)image:(UIImage *)image
finishedSavingWithError:(NSError *)error
  contextInfo:(void *)contextInfo {
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Save failed"
                              message: @"Failed to save image"
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
}

@end
