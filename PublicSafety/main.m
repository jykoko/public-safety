//
//  main.m
//  PublicSafety
//
//  Created by Jacob Koko on 10/15/14.
//  Copyright (c) 2014 we.engineer.net.jykoko. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool
    {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
