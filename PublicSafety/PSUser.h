//
//  NSObject+User.h
//  PublicSafety
//
//  Created by Jacob Koko on 10/16/14.
//  Copyright (c) 2014 we.engineer.net.jykoko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PSRequestHandler.h"

@interface PSUser : NSObject

-(void)setProfileImage:(NSString*)pictureDataString;
-(id)init;

@property PSRequestHandler* userRequestHandler;
@property(strong) NSString* userFirstName;
@property(strong) NSString* userLastName;
@property(strong) NSString* userType;
@property(strong) NSString* userEmail;
@property(strong) NSString* userImagePath;
@property long userId;
@property long userSchoolID;
@property(strong) NSNumber* userPhoneNumb;
@property(strong) NSNumber* userLatitude;
@property(strong) NSNumber* userLongitude;
@property(strong) UIImage* userImage;
@property(strong) NSString* userOrganizationIdentifierLable;
@property(strong) NSDictionary* userJsonResponse;
@property(strong) NSString* userUserName;
@property(strong) NSString* userSchool;
@property(strong) NSString* userClientSecretToken;


@end
