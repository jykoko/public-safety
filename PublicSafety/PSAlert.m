//
//  PSAlert.m
//  PublicSafety
//
//  Created by Admin on 10/26/14.
//  Copyright (c) 2014 we.engineer.net.jykoko. All rights reserved.
//

#import "PSAlert.h"

@interface PSAlert()
//private instances go here...
@end

@implementation PSAlert

@synthesize alertID = _alertID;
@synthesize alertSchoolID = _alertSchoolID;
@synthesize alertTitle = _alertTitle;
@synthesize alertColor = _alertColor;
@synthesize alertEnable = _alertEnable;
@end
