//
//  UIViewController+RegistrationStepTwoViewController.h
//  PublicSafety
//
//  Created by Jacob Koko on 11/15/14.
//  Copyright (c) 2014 we.engineer.net.jykoko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegistrationStepTwoViewController : UIViewController <UITextFieldDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UIAlertViewDelegate>
@property (strong, nonatomic) IBOutlet UITextField *userOrgIdField;
@property (strong, nonatomic) IBOutlet UIButton *registerButton;
@property (strong, nonatomic) IBOutlet UITextField *userConfirmationPinField;

@end
