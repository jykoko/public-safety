//
//  PSAlert.h
//  PublicSafety
//
//  Created by Admin on 10/26/14.
//  Copyright (c) 2014 we.engineer.net.jykoko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface PSAlert : NSObject

@property(strong) NSNumber* alertID;
@property(strong) NSNumber* alertSchoolID;
@property(strong) NSString* alertTitle;
@property(strong) NSString* alertColor;
@property BOOL* alertEnable;

@end