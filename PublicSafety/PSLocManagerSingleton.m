//
//  LocationManagerSingleton.m
//  SkateFinder
//
//  Created by Jacob Koko on 8/6/14.
//
#import "PSLocManagerSingleton.h"
#import "PSImageCaptureMediaHandler.h"

@implementation PSLocManagerSingleton

@synthesize locationManager,userLocations,startingLoc,distanceTraveled;

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

//image capture handler
PSImageCaptureMediaHandler* imgHandler;
NSDate* startTime;

- (id)init
{
    self = [super init];
    
    if(self)
    {
        self.distanceTraveled = 0;
        self.userLocations = [[NSMutableArray alloc]init];
        self.locationManager = [CLLocationManager new];
        [self.locationManager setDelegate:self];
        [self.locationManager setDistanceFilter:kCLDistanceFilterNone];
        [self.locationManager setHeadingFilter:kCLHeadingFilterNone];
        
        if(IS_OS_8_OR_LATER)
        {
            [self.locationManager requestAlwaysAuthorization];
        }
        
        [self.locationManager startUpdatingLocation];
        
        startTime = [NSDate date];
    }
    
    return self;
}

+ (PSLocManagerSingleton*)sharedSingleton
{
    static PSLocManagerSingleton* sharedSingleton;
    if(!sharedSingleton)
    {
        @synchronized(sharedSingleton)
        {
            sharedSingleton = [PSLocManagerSingleton new];
        }
    }
    
    return sharedSingleton;
}

- (void)clearUserLocations
{
    if(self.userLocations != nil)
    {
         [self.userLocations removeAllObjects];
         NSLog(@"walk data cleared.");
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    if(!self.startingLoc)
    {
        self.startingLoc = newLocation;
    }
    
//    NSDate* curTime = [NSDate date];
//    if(startTime == nil)
//    {
//        startTime = curTime;
//    }
//    
//    NSTimeInterval secondsSinceStart = [curTime timeIntervalSinceDate:startTime];
//    NSLog(@"Seconds since last location update --------> %f", secondsSinceStart);
//    if(secondsSinceStart >= 3)
//    {
        CLLocationDistance metersTraveled = [self.startingLoc distanceFromLocation:newLocation];
        
        //using meters
        //distanceTraveled = metersTraveled;
        
        //using miles
        distanceTraveled = (metersTraveled/1609.344);
        
        NSLog(@"dist traveled: %f", distanceTraveled);
        
        NSLog(@"updating loc: %f / %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
        [self.userLocations addObject:newLocation];
        
       // secondsSinceStart = 0;
        //startTime = [NSDate date];
 //   }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading
{
    //handle your heading updates here- I would suggest only handling the nth update, because they
    //come in fast and furious and it takes a lot of processing power to handle all of them
}

@end