//
//  PSReport.m
//  PublicSafety
//
//  Created by Admin on 10/26/14.
//  Copyright (c) 2014 we.engineer.net.jykoko. All rights reserved.
//

#import "PSReport.h"


@interface PSReport()
//private instances go here...
@end

@implementation PSReport

@synthesize reportID = _reportID;
@synthesize reportUserID = _reportUserID;
@synthesize reportAlertID = _reportAlertID;
@synthesize reportStatusID = _reportStatusID;
@synthesize reportLongitude = _reportLongitude;
@synthesize reportLatitude = _reportLatitude;

@end
