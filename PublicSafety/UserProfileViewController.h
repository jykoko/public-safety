//
//  UIViewController+UserProfileViewController.h
//  PublicSafety
//
//  Created by Jacob Koko on 10/28/14.
//  Copyright (c) 2014 we.engineer.net.jykoko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "PSUser.h"

@interface UserProfileViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, MBProgressHUDDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
- (IBAction)logOut:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *userStatsTableView;
@property (strong, nonatomic) IBOutlet UIImageView *userProfileImageView;
@property (strong, nonatomic) IBOutlet UILabel *userNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *userOrganizationIdentifierLable;
@property PSUser* currentUser;
@end
