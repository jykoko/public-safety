//
//  UIViewController+MainViewController.h
//  PublicSafety
//
//  Created by Jacob Koko on 10/28/14.
//  Copyright (c) 2014 we.engineer.net.jykoko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KAProgressLabel.h"

@interface MainViewController : UIViewController <UIGestureRecognizerDelegate,UICollectionViewDelegate,UICollectionViewDataSource,
    UITabBarDelegate>
{
   
}

- (IBAction)showAlertInformation:(id)sender;
- (IBAction)alertCampusPolice:(id)sender;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *showAlertInfo;
@property (strong, nonatomic) IBOutlet UITabBar *mainViewTabBar;
@property (strong, nonatomic) IBOutlet UIView *collectionViewBgView;
@property NSMutableArray *imagesArray;
@property (strong, nonatomic) IBOutlet UICollectionView *imagesCollection;
@property (strong, nonatomic) IBOutlet UIImageView *lastshotview;
@property (strong, nonatomic) IBOutlet KAProgressLabel *myAlertLabel;
@property (strong, nonatomic) IBOutlet UIButton *myAlertButton;

@end
