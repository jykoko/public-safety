//
//  UIViewController+SettingsViewController.m
//  PublicSafety
//
//  Created by Christian Decker on 11/16/14.
//  Copyright (c) 2014 we.engineer.net.jykoko. All rights reserved.
//

#import "SettingsViewController.h"
#import "TutorialViewController.h"

@implementation SettingsViewController
@synthesize userPinTexBox, userLabel;

-(void)viewDidLoad
{
    userPinTexBox.delegate = self;
    self.navigationItem.title = @"Settings";
    
    //Adds a backbutton
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style: UIBarButtonItemStyleBordered target:self action:@selector(Back)];
    //Adds the custom red color to the back button
    [backButton setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor redColor]} forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = backButton;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    
    //Shows that the pin is saved
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if(defaults)
    {
        NSString *passcode = [defaults objectForKey:@"securityPin"];
        userLabel.text = passcode;
        userPinTexBox.text = passcode;
    }
    
    [super viewDidLoad];
}

-(IBAction)setUserSettings:(id)sender
{   
    NSString *passcode = [userPinTexBox text];
    
    //Checks if the pass codes is equal to 4, if not it will display an alert view
    if(passcode.length == 4)
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        [defaults setObject:passcode forKey:@"securityPin"];
        [defaults synchronize];
        
        NSLog(@"Data saved");
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Pin Length"
                                                        message:@"Your pin must be four digits"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

- (IBAction)Back
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)displayTutorialVc:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    TutorialViewController *tutVc = [storyboard instantiateViewControllerWithIdentifier:@"TutorialViewController"];
    [self presentViewController:tutVc animated:YES completion:nil];
}
@end
