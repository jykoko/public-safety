//
//  TutorialViewController.h
//  PublicSafety
//
//  Created by Jacob Koko on 11/24/14.
//  Copyright (c) 2014 we.engineer.net.jykoko. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "PageContentViewController.h"

@interface TutorialViewController : UIViewController <UIPageViewControllerDataSource>

- (IBAction)startWalkthrough:(id)sender;
@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) NSArray *pageTitles;
@property (strong, nonatomic) NSArray *pageImages;

@end
