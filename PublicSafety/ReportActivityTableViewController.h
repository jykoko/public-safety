//
//  UITableViewController+ReportActivityTableViewController.h
//  PublicSafety
//
//  Created by Jacob Koko on 11/13/14.
//  Copyright (c) 2014 we.engineer.net.jykoko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface  ReportActivityTableViewController: UITableViewController<UITableViewDataSource,UITableViewDelegate, UIAlertViewDelegate>

@end
