//
//  PSPicture.h
//  PublicSafety
//
//  Created by Antonio Garcia on 10/26/14.
//  Copyright (c) 2014 we.engineer.net.jykoko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PSRequestHandler.h"

@interface PSPicture : NSObject

-(void)getAllPictures;

@property(strong) NSNumber* pictureId;
@property(strong) NSNumber* pictureImagePath;


@end
