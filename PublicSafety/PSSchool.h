//
//  PSSchool.h
//  PublicSafety
//
//  Created by Antonio Garcia on 10/26/14.
//  Copyright (c) 2014 we.engineer.net.jykoko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PSRequestHandler.h"

@interface PSSchool : NSObject

-(void)getAllSchools;

@property(strong) NSString* schoolEmail;
@property(strong) NSNumber* schoolId;
@property(strong) NSNumber* schoolPhoneNumb;
@property(strong) NSNumber* schoolAddress;


@end
