//
//  UICollectionViewCell+ImageCell.m
//  PublicSafety
//
//  Created by Jacob Koko on 11/22/14.
//  Copyright (c) 2014 we.engineer.net.jykoko. All rights reserved.
//

#import "PSImageCell.h"

@implementation PSImageCell

@synthesize parseImage, loadingSpinner;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        //self.parseImage.layer.borderWidth = 3.0f;
        // self.parseImage.layer.borderColor = [UIColor whiteColor].CGColor;
    }
    return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end
