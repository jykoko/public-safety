//
//  UIViewController+ShareMyWalkViewController.h
//  PublicSafety
//
//  Created by Jacob Koko on 11/2/14.
//  Copyright (c) 2014 we.engineer.net.jykoko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "MBProgressHUD.h"

@interface ShareMyWalkViewController : UIViewController <MKMapViewDelegate,CLLocationManagerDelegate,MBProgressHUDDelegate,UIAlertViewDelegate>

@property (strong, nonatomic) UISegmentedControl *sharedWalkSegControl;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *shareWalkBtn;
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) MKPolyline* routeLine;
@property (nonatomic, strong) MKPolylineView* routeLineView;
@property (nonatomic, strong) NSMutableArray *trackPointArray;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, readwrite) MKMapRect routeRect;
@property NSMutableArray* userLocations;
@property MBProgressHUD* progHud;

- (IBAction)shareWalk:(id)sender;


@end
