//
//  UIViewController+MainViewController.m
//  PublicSafety
//
//  Created by Jacob Koko on 10/28/14.
//  Copyright (c) 2014 we.engineer.net.jykoko. All rights reserved.
#import "MainViewController.h"
#import "PSUser.h"
#import "PSRequestHandler.h"
#import "SWRevealViewController.h"
#import "PSConstants.h"
#import "OtherAlertsViewController.h"
#import "PSLocManagerSingleton.h"
#import "PSCountDownAlertView.h"
#import "PSImageCaptureMediaHandler.h"
#import "PSImageCell.h"
#import "ShareMyWalkViewController.h"
#import "ReportActivityTableViewController.h"
#import "TutorialViewController.h"

const int HIGH_PRIORITY_ALERT_ID = 10;

@interface MainViewController()
{
    float counter;
}
@end

@implementation MainViewController
@synthesize myAlertLabel = _myAlertLabel;
@synthesize myAlertButton = _myAlertButton;
@synthesize lastshotview,imagesCollection,imagesArray,collectionViewBgView,mainViewTabBar;

//private class vars
NSTimer* timer;
PSLocManagerSingleton* locManager;
PSImageCaptureMediaHandler* imgHandler;
CLLocation* currentLoc;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationItem setHidesBackButton:YES];
     //self.navigationController.navigationBar.barTintColor = [UIColor redColor];
    [self setupBlurredFooterView];
    
    self.mainViewTabBar.delegate = self;
    [self.mainViewTabBar setTintColor:[UIColor redColor]];
    
    self.imagesCollection.delegate = self;
    self.imagesCollection.dataSource = self;
    self.imagesCollection.allowsSelection = YES;
    
    imgHandler = [[PSImageCaptureMediaHandler alloc]init];
    
    //Create custom menu button for bar
    UIImage *background = [UIImage imageNamed:@"menuList.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [button setBackgroundImage:background forState:UIControlStateNormal];
    button.frame = CGRectMake(0 ,0,32,32);
    
    //Add custom button as view on top of uibarbutton, and add to navr bar
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButton;
    
    //Set the gesture for slide out menu
    //[self.view addGestureRecognizer:swipeLeft];
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    //set up alert label for circular progress bar
    [_myAlertLabel setColorTable: @{
                   NSStringFromProgressLabelColorTableKey(ProgressLabelFillColor):[UIColor clearColor],
                   NSStringFromProgressLabelColorTableKey(ProgressLabelTrackColor):[UIColor redColor],
                   NSStringFromProgressLabelColorTableKey(ProgressLabelProgressColor):[UIColor greenColor]
    }];
    [_myAlertLabel setBackBorderWidth:10.0];
    [_myAlertLabel setFrontBorderWidth:10.0];
    
    [_myAlertButton addTarget:self
                       action:@selector(touchDown:)
             forControlEvents:UIControlEventTouchDown];
    [_myAlertButton addTarget:self
               action:@selector(touchUp:)
     forControlEvents:UIControlEventTouchUpInside|UIControlEventTouchUpOutside|UIControlEventTouchCancel];
    
    //Initialize location manager singleton
    currentLoc = [PSLocManagerSingleton sharedSingleton].locationManager.location;
    
    //start image capturing
    [PSImageCaptureMediaHandler sharedSingleton];
    
    [self setupBlurredFooterView];
    [self setNeedsStatusBarAppearanceUpdate];
    
    //only show when user first logs in to app for first time
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"hasSeenTutorial"])
    {
        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
        [defaults setBool:YES forKey:@"hasSeenTutorial"];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        TutorialViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"TutorialViewController"];
        [self presentViewController:viewController animated:YES completion:nil];
    }
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)touchUp:(id)sender
{
    [timer invalidate];
    counter=0.0;
}

- (IBAction)touchDown:(id)sender
{
    NSLog(@"down %f",counter);
    timer = [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(incrementBar) userInfo:nil repeats:YES];
}

//progress bar increment function that controls speed of progress spinner
-(void)incrementBar
{
    if(counter >= 1.0)
    {
        __weak typeof(self) weakSelf = self;
        _myAlertLabel.progressLabelVCBlock = ^(KAProgressLabel *label, CGFloat progress)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [label setText:[NSString stringWithFormat:@"Sending - %.0f%%", (progress*100)]];
                
                if(progress >= 1.0)
                {
                    [weakSelf alertCampusPolice:nil];
                    UIAlertView* alertSent=[[UIAlertView alloc]initWithTitle:@"Alert Sent" message:@"Your emergency alert has been sent, help will be on the way shortly." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alertSent show];
                    
                    label.text=@"Alert Sent!";
                    return;
                }
            });
        };
        
        counter=0;
    }
    else
    {
        counter+=0.1;
    }
    
    [_myAlertLabel setProgress:counter timing:TPPropertyAnimationTimingLinear duration:0.2 delay:0];
}


- (IBAction)showAlertInformation:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    OtherAlertsViewController *otherVc = [storyboard instantiateViewControllerWithIdentifier:@"OtherAlertsViewController"];
    [self.navigationController presentViewController:otherVc animated:YES completion:nil];
}

//alert campus police when alert fired
- (IBAction)alertCampusPolice:(id)sender
{
    //safety check, we want to make sure location isnt nil before POST occurs
    if(!currentLoc)
    {
        currentLoc = [PSLocManagerSingleton sharedSingleton].locationManager.location;
    }
    
    //format time stamp for database update
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString* dateString = [dateFormatter stringFromDate:[NSDate date]];
    
    //POST report to database using request handler
    NSNumber *alertID = [NSNumber numberWithLong:HIGH_PRIORITY_ALERT_ID];
    [PSRequestHandler addNewReport:dateString andLocations:[PSLocManagerSingleton sharedSingleton].userLocations andAlertID:alertID andCompletion:^(NSDictionary *responseDictionary) {
        if(responseDictionary)
        {
            NSString* lastIDInserted = [responseDictionary objectForKey:@"ID"];
            int reportId = [lastIDInserted intValue];
            NSLog(@"Report Id -> %i",reportId);
            
            
            for(int i = 0 ; i < [imgHandler.walkLiveImageDataArray count]; i++)
            {
                NSData* picData = [imgHandler.walkLiveImageDataArray objectAtIndex:i];
                [PSRequestHandler addNewPictureToReport:picData andReportId:reportId andCompletion:^ (NSDictionary *responseDict) {
                    if(responseDict)
                    {
                        NSLog(@"img # %i sent... success", i);
                    }
                }];
            }
            
            //clear out locations from memory
            [[PSLocManagerSingleton sharedSingleton] clearUserLocations];
            [imgHandler clearWalkImages];
            [imgHandler clearWalkImageDataArray];
        }
    }];
    
    [self.imagesCollection reloadData];
    
    //display custom count down view
    PSCountDownAlertView *alert = [[PSCountDownAlertView alloc] initWithFrame:CGRectMake(5, 100, 300, 200)];
    [self.view addSubview:alert];
    [alert show];
    
    NSLog(@"campus police alerted...");
}

#pragma mark - MKMapViewDelegate

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    static NSString *GeoPointAnnotationIdentifier = @"RedPin";
    
    MKPinAnnotationView *annotationView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:GeoPointAnnotationIdentifier];
    
    if (!annotationView) {
        annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:GeoPointAnnotationIdentifier];
        annotationView.pinColor = MKPinAnnotationColorPurple;
        annotationView.canShowCallout = YES;
        annotationView.animatesDrop = YES;
    }
    
    return annotationView;
}

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay
{
    MKCircleView *circleView = [[MKCircleView alloc] initWithOverlay:overlay];
    circleView.strokeColor = [UIColor colorWithRed:00.0f/255.0f green:172.0f/255.0f blue:237.0f/255.0f alpha:1.0f];
    circleView.fillColor = [UIColor colorWithWhite:0.3f alpha:0.3f];
    circleView.lineWidth = 2.0f;
    
    return circleView;
}


#pragma mark - UICollectionView data source

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if([imgHandler.walkLiveImages count] == 0)
    {
        return 1;
    }
    else
    {
        return [imgHandler.walkLiveImages count];
    }
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"image_cell";
    PSImageCell* cell = (PSImageCell*)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.parseImage.layer.cornerRadius =  cell.parseImage.frame.size.height/2;
    cell.parseImage.layer.masksToBounds = YES;
    cell.parseImage.layer.borderWidth = 2;
    [cell.parseImage.layer setBorderColor: [[UIColor redColor] CGColor]];
    [cell.parseImage setHidden:YES];
    
    if ([imgHandler.walkLiveImages count] > 0)
    {
        [cell.parseImage setHidden:NO];
        cell.parseImage.image = [imgHandler.walkLiveImages objectAtIndex:indexPath.row];
        NSLog(@"setting image cell %@",[imgHandler.walkLiveImages objectAtIndex:indexPath.row]);
        [cell.loadingSpinner stopAnimating];
    }
    
    NSLog(@"setting image cell");
    
    return cell;
}

/**
 * Fix this, not being called!!!
 */
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    [cell setHighlighted:YES];
    [cell setSelected:YES];
    
    if([self.imagesArray count] != 0)
    {
        //[self enlargeThePhoto:nil];
    }
}

- (void)setupBlurredFooterView
{
    UIVisualEffect *blurEffect;
    blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
    
    UIVisualEffectView *visualEffectView;
    visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    
    visualEffectView.frame = self.collectionViewBgView.bounds;
    [self.collectionViewBgView insertSubview:visualEffectView atIndex:0];
}

#pragma tab bar delegates

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    if([item.title isEqualToString:@"Share Walk"])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        ShareMyWalkViewController *shareVc = [storyboard instantiateViewControllerWithIdentifier:@"ShareMyWalkViewController"];
        UINavigationController *navigationController =
        [[UINavigationController alloc] initWithRootViewController:shareVc];
        
        //now present this navigation controller modally
        [self presentViewController:navigationController
                           animated:YES
                         completion:^{
                             
                         }];
    }
    else if([item.title isEqualToString:@"Report Activity"])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        ReportActivityTableViewController *reportVc = [storyboard instantiateViewControllerWithIdentifier:@"ReportActivityTableViewController"];
        UINavigationController *navigationController =
        [[UINavigationController alloc] initWithRootViewController:reportVc];
        
        //now present this navigation controller modally
        [self presentViewController:navigationController
                           animated:YES
                         completion:^{
                             
                         }];
    }
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

@end