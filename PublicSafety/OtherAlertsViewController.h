//
//  UIViewController+OtherAlertsViewController.h
//  PublicSafety
//
//  Created by Jacob Koko on 11/2/14.
//  Copyright (c) 2014 we.engineer.net.jykoko. All rights reserved.
//
#import <UIKit/UIKit.h>

@interface OtherAlertsViewController : UIViewController <UICollectionViewDelegate,UICollectionViewDataSource>

- (IBAction)hideViewController:(id)sender;

@property (strong, nonatomic) IBOutlet UICollectionView *imagesCollection;
@property NSMutableArray *imagesArray;
@property (strong, nonatomic) IBOutlet UILabel *distTraveledLabel;
@property (strong, nonatomic) IBOutlet UIButton *hideView;

@end
