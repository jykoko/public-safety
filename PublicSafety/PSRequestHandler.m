/*
 * This class is a wrapper around AFNetworking, and it stores ALL of the GET/POST/PUT
 * functions needed for the app. We use asychronous calls on background threads to
 * prevent slow load times. The methods are designed to mock the Parse SDK that everyone
 * is already familiar with, so it uses completion blocks.
 */
#import "PSRequestHandler.h"
#import "AFNetworking.h"
#import "PSConstants.h"
#import <MapKit/MapKit.h>
#import "PSUser.h"
@interface PSRequestHandler()
//private instance vars
@end

@implementation PSRequestHandler
@synthesize lastInsertId;

int SHARE_WALK_PRIORITY = 0;
int HIGH_ALERT_PRIORITY = 1;

#pragma - Authentication Requests
+ (void)attemptLoginWithUserName:(NSString *)userName andPassWord:(NSString *)passWord andCompletion:(void (^)(NSDictionary *))completion
{
    NSURL *url = [NSURL URLWithString:PSLoginApiBaseUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    
    NSDictionary* userData =  @{
                                @"Username":userName,
                                @"Password":passWord
                               };
    
    NSError *error = nil;
    NSData *json;
    if ([NSJSONSerialization isValidJSONObject:userData])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:userData options:0 error:&error];
        
        // If no errors, POST the json
        if (json != nil && error == nil)
        {
            [request setHTTPMethod:@"POST"];
            [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
            [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[json length]] forHTTPHeaderField:@"Content-Length"];
            [request setHTTPBody:json];
            
            [NSURLConnection sendAsynchronousRequest:request
                                               queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
             {
                 NSLog(@"API Response is: %@",[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding]);
                 NSError* errorJson = nil;
                 NSDictionary* responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorJson];
                 NSLog(@"attempting login...");
                 completion(responseDict);
             }];
        }
    }
}

#pragma - Alert API Requests
/*
 * POST new alert
 * @Param: Pass in variables rather than hard coded values like below
 *         NSString* outString = @"ID=111&SchoolID=11&Color=777&Enabled=true";
 */
+ (void)addNewAlert:(NSString*)alertId andSchoolID:(NSNumber*)schoolID andColor:(NSString*)color
                                                               andEnabledStatus:(BOOL)enabledStatus
                                                                      andAPIUrl:(NSString*)urlString
{
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    
    //format output string based on bool value
    NSString* boolVal = @"false";
    if(enabledStatus)
    {
        boolVal=@"true";
    }
    
    NSString* outString = [NSString stringWithFormat:@"ID=%@&SchoolID=%@&Color=%@&Enabled=%@",alertId,schoolID,color,boolVal];
    NSData *requestData = [outString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"out data: %@",outString);
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody: requestData];
    
    [[NSURLConnection alloc] initWithRequest:request delegate:self];
}

+ (void)getAlerts:(void(^)(NSDictionary *dictionary))completion
{
    NSString *schoolQuery = [NSString stringWithFormat: @"%@?School=%i", PSAlertApiBaseUrl , 3];
    NSURL *url = [NSURL URLWithString:schoolQuery];
    NSLog(@"%@", url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];

    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         NSLog(@"API Response is: %@",[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding]);
         
         if (!data)
         {
             NSLog(@"Download Error: %@", error.localizedDescription);
             UIAlertView *alert =
             [[UIAlertView alloc]initWithTitle:@"Error"
                                       message:[NSString stringWithFormat:@"Error : %@",error.localizedDescription]
                                      delegate:self
                             cancelButtonTitle:@"Ok"
                             otherButtonTitles:nil];
             [alert show];
         }
         
         //Parsing the JSON data received from web service into an NSDictionary object
         NSArray *JSON =
         [NSJSONSerialization JSONObjectWithData: data
                                         options: NSJSONReadingMutableContainers
                                           error: &error];
         
         NSMutableDictionary *JSONReturn =[[NSMutableDictionary alloc]init];
         for(int i = 0 ; i < [JSON count]; i++)
         {
             int priority = [[[JSON objectAtIndex:i] objectForKey:@"Priority"]intValue];
             if(priority > 1)
             {
                  [JSONReturn setValue:[[JSON objectAtIndex:i] objectForKey:@"Title"] forKey:[[JSON objectAtIndex:i] objectForKey:@"ID"]];
             }
         }

         completion(JSONReturn);
     }];
}

#pragma - User API Requests

/*
 * POST new user
 * @Param: Pass in variables rather than hard coded values like below
 *  NSString* outString=@"FName=Ferd&ID=111&LName=Berfel&OrganizationIdentifier=21&Password=password&
 *                        ProfilePicId=25&RoleId=3&SchoolId=10&Username=ferdBerfelllllly";
 */
+(void)addNewUser:(NSString*)userName andPassword:(NSString*)
passWord andRoleId:(NSNumber*)roleID andFName:(NSString*)fName andLName:(NSString*)lName
      andSchoolID:(NSNumber*)schoolID andPictureId:(NSNumber*)pictureID andOrgID:(NSString*)orgID andCompletion:(void (^)(NSDictionary* jsonResponse))completion
{
    
    NSURL *url = [NSURL URLWithString:PSUserApiBaseUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    
    long role = [roleID longValue];
    long pic = [pictureID longValue];
    long school = [schoolID longValue];
    
    NSDictionary* params = @{
                                @"Username":userName,
                                @"Password":passWord,
                                @"Role":@{@"ID":[NSNumber numberWithLong:role]},
                                @"FName":fName,
                                @"LName":lName,
                                @"School":@{@"ID":[NSNumber numberWithLong:school]},
                                @"Picture":@{@"ID":[NSNumber numberWithLong:pic]},
                                @"OrganizationIdentifier":orgID
                            };
    
    NSError *error = nil;
    NSData *json;
    
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:params])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:&error];
        
        // If no errors, POST the json
        if (json != nil && error == nil)
        {
            [request setHTTPMethod:@"POST"];
            [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
            [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[json length]] forHTTPHeaderField:@"Content-Length"];
            [request setHTTPBody:json];
            
            [NSURLConnection sendAsynchronousRequest:request
                                               queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
             {
                 NSLog(@"API Response is: %@",[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding]);
                 NSError* errorJson = nil;
                 NSDictionary* responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorJson];
                 
                 completion(responseDict);
             }];
        }
    }
}


/*
 * GET all users
 * NOTE: This is an asynchronous call!
 */
+(void)getAllUsersFromJSON:(NSString*)urlString andCompletion:(void (^)(NSDictionary* jsonResponse))completion
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    [manager GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //set JSON dictionary
        if(responseObject != nil && [responseObject count] > 0)
        {
            NSDictionary* JSON = [responseObject objectAtIndex:0];
            NSLog(@"JSON response: %@",JSON);
            if(completion)
            {
                completion(JSON);
            }
        }
            
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        NSLog(@"Error: %@", error);
            
    }];
}

/*
 * GET specific user
 * NOTE: This is an asynchronous call!
 */

+ (void)getUserFromJSON:(int)userID andCompletion:(void (^)(NSDictionary* jsonResponse))completion
{
    NSString* userUrl = [NSString stringWithFormat:@"%@/%i",PSUserApiBaseUrl,userID];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    [manager GET:userUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"response: %@",responseObject);
        if(completion){
            completion(responseObject);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        
    }];
}

/*
+(void)getUserFromJSON:(NSString*)userName andUrlString:(NSString*)urlString andCompletion:(void (^)(NSDictionary* jsonResponse))completion
{
    //format URL for API to get only one specific user based on username
    NSString* urlForSpecificUser = [NSString stringWithFormat:@"%@/%@",urlString,userName];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    [manager GET:urlForSpecificUser parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //set JSON dictionary
        NSDictionary *JSON = [responseObject objectAtIndex:0];
        NSLog(@"response: %@",responseObject);
        if(completion){
            completion(JSON);
        }
            
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        NSLog(@"Error: %@", error);
            
    }];
}
*/

#pragma - Report API Requests
/*
 * POST new report
 * NOTE: This is an asynchronous call!
 */
+(void)addNewReport:(NSString*)dateString andLocations:(NSMutableArray*)locations andAlertID:(NSNumber*)AlertID andCompletion:(void (^)(NSDictionary *))completion
{
    NSURL *url = [NSURL URLWithString:PSReportApiBaseUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    
    NSMutableArray* arrayOfLocs = [[NSMutableArray alloc]init];
    // Array of locations for json formatting
    for(int i= 0; i < [locations count]; i++)
    {
        CLLocation* loc = [locations objectAtIndex:i];
        NSDictionary* locs =  @{   @"Longitude":[NSNumber numberWithFloat:loc.coordinate.longitude],
                                   @"Latitude":[NSNumber numberWithFloat:loc.coordinate.latitude],
                                   @"Time":dateString,
                                   };
        [arrayOfLocs addObject:locs];
    }
    
    PSUser* currentUser = [[PSUser alloc]init];
    NSDictionary* params = @{   @"School":@{ @"ID":[NSNumber numberWithLong:currentUser.userSchoolID] },
                                @"Alert": @{ @"ID": AlertID },
                                @"Status":@{ @"ID":[NSNumber numberWithLong:17] },
                                @"User":  @{ @"ID":[NSNumber numberWithLong:currentUser.userId] },
                                @"Locations": arrayOfLocs
                            };
    
    NSError *error = nil;
    NSData *json;
    
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:params])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:&error];
        
        // If no errors, POST the json
        if (json != nil && error == nil)
        {
            [request setHTTPMethod:@"POST"];
            [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
            [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[json length]] forHTTPHeaderField:@"Content-Length"];
            [request setHTTPBody:json];
            
            [NSURLConnection sendAsynchronousRequest:request
                                               queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
             {
                 NSLog(@"API Response is: %@",[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding]);
                 NSError* errorJson = nil;
                 NSDictionary* responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorJson];
                 
                 completion(responseDict);
             }];
        }
    }
}

+ (void)addNewShareWalkReport:(float)latitude andLongitude:(float)longitude andDateString:(NSString *)dateString andReportId:(int)reportId
{
    NSString* formattedUrl = [NSString stringWithFormat:@"http://caffeinatedcm-001-site3.smarterasp.net/api/v1/Report/%i/Location",reportId];
    NSURL *url = [NSURL URLWithString:formattedUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];

    NSDictionary* locs =  @{   @"Longitude":[NSNumber numberWithFloat:longitude],
                               @"Latitude":[NSNumber numberWithFloat:latitude],
                               @"Time":dateString,
                            };

    NSError *error = nil;
    NSData *json;
    
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:locs])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:locs options:NSJSONWritingPrettyPrinted error:&error];
        
        // If no errors, POST the json
        if (json != nil && error == nil)
        {
            [request setHTTPMethod:@"POST"];
            [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
            [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[json length]] forHTTPHeaderField:@"Content-Length"];
            [request setHTTPBody:json];
            
            [NSURLConnection sendAsynchronousRequest:request
                                               queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
             {
                 NSLog(@"API Response is: %@",[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding]);
                 NSError* errorJson = nil;
                 NSDictionary* responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorJson];
                 NSLog(@"posting user coordinate...");
             }];
        }
    }

}


#pragma - School API Requests
/*
 * POST School
 * @Param: Pass in variables rather than hard coded values like below
 */
+ (void)addNewSchool:(NSNumber*)schoolId andSchoolEmail:(NSString*)schoolEmail
    andSchoolAddress:(NSString*)schoolAddress andSchoolPhoneNumber:(NSString*)schoolPhoneNumber
{
    //stubbed out method for now
}

/*
 * GET School
 * @Param: Pass in variables rather than hard coded values like below
 */
+(void)getSchoolFromJSON:(NSNumber *)schoolId andUrlString:(NSString *)urlString andCompletion:(void (^)(NSDictionary* jsonResponse))completion
{
    //format URL for API to get only one specific user based on school Id
    NSString* urlForSpecificUser = [NSString stringWithFormat:@"%@/%@",urlString,schoolId];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    [manager GET:urlForSpecificUser parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //set JSON dictionary
        NSDictionary *JSON = [responseObject objectAtIndex:0];
        NSLog(@"response: %@",responseObject);
        if(completion){
            completion(JSON);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        
    }];
}

/*
 * GET All Schools
 * @Param: Pass in variables rather than hard coded values like below
 */
+(void)getAllSchoolsFromJSON:(NSString*)urlString andCompletion:(void (^)(NSDictionary* jsonResponse))completion
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    [manager GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //set JSON dictionary
        NSDictionary* JSON = [responseObject objectAtIndex:0];
        NSLog(@"JSON response: %@",JSON);
        if(completion){
            completion(JSON);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        
    }];
}

#pragma - Picture API Requests
/*
 * GET Picture
 * @Param: Pass in variables rather than hard coded values like below
 */
+ (void)getPictureFromJSON:(int)pictureID andCompletion:(void (^)(NSDictionary* jsonResponse))completion
{
    NSString* imgUrl = [NSString stringWithFormat:@"%@/%i", PSPictureApiBaseUrl, pictureID];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    [manager GET:imgUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if(completion){
            completion(responseObject);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        
    }];
}

/*
+(void)getPictureFromJSON:(NSNumber *)pictureId andUrlString:(NSString *)urlString andCompletion:(void (^)(NSDictionary* jsonResponse))completion
{
    //format URL for API to get only one specific user based on school Id
    NSString* urlForSpecificUser = [NSString stringWithFormat:@"%@/%@",urlString,pictureId];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    [manager GET:urlForSpecificUser parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //set JSON dictionary
        NSDictionary *JSON = [responseObject objectAtIndex:0];
        NSLog(@"response: %@",responseObject);
        if(completion){
            completion(JSON);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        
    }];
}
*/

/*
 * GET all Pictures
 * @Param: Pass in variables rather than hard coded values like below
 */
+(void)getAllPicturesFromJSON:(NSString*)urlString andCompletion:(void (^)(NSDictionary* jsonResponse))completion
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    [manager GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"comp: %lu",(unsigned long)[responseObject count]);
        if(completion)
        {
            completion(responseObject);
        }
        
    }
    failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
        
        NSLog(@"Error: %@", error);
        
    }];
}

/*
 * GET photo
 * @Param: Pass in variables rather than hard coded values like below
 */
+ (void)addNewPicture:(NSData*)pictureData andCompletion:(void (^)(NSDictionary *responseDict))completion
{
    NSURL *url = [NSURL URLWithString:PSPictureApiBaseUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    
    NSString *encodedstring = [pictureData base64EncodedStringWithOptions:kNilOptions];
    NSDictionary* pic =  @{
                              @"Data":encodedstring
                          };
    
    NSError *error = nil;
    NSData *json;
    if ([NSJSONSerialization isValidJSONObject:pic])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:pic options:0 error:&error];

        // If no errors, POST the json
        if (json != nil && error == nil)
        {
            [request setHTTPMethod:@"POST"];
            [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
            [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[json length]] forHTTPHeaderField:@"Content-Length"];
            [request setHTTPBody:json];
            
            [NSURLConnection sendAsynchronousRequest:request
                                               queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
             {
                 NSLog(@"API Response is: %@",[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding]);
                 NSError* errorJson = nil;
                 NSDictionary* responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorJson];
                 completion(responseDict);
             }];
        }
    }
}


+ (void)addNewPictureToReport:(NSData*)pictureData andReportId:(int)reportID andCompletion:(void (^)(NSDictionary *responseDict))completion
{
    NSString* formattedUrl = [NSString stringWithFormat:@"http://caffeinatedcm-001-site3.smarterasp.net/api/v1/Report/%i/Picture",reportID];
    NSURL *url = [NSURL URLWithString:formattedUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    
    NSString *encodedstring = [pictureData base64EncodedStringWithOptions:kNilOptions];
    NSDictionary* pic =  @{
                           @"Data":encodedstring
                          };
    
    NSError *error = nil;
    NSData *json;
    if ([NSJSONSerialization isValidJSONObject:pic])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:pic options:0 error:&error];
        
        // If no errors, POST the json
        if (json != nil && error == nil)
        {
            [request setHTTPMethod:@"POST"];
            [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
            [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[json length]] forHTTPHeaderField:@"Content-Length"];
            [request setHTTPBody:json];
            
            [NSURLConnection sendAsynchronousRequest:request
                                               queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
             {
                 NSLog(@"Image API Response is: %@",[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding]);
                 NSError* errorJson = nil;
                 NSDictionary* responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorJson];
                 NSLog(@"posting user image...");
                 completion(responseDict);
             }];
        }
    }
}

#pragma - Location API Requests
/*
 * POST Location
 * @Param: Pass in variables rather than hard coded values like below
 */
+ (void)addNewLocation:(NSNumber*)LocationID andLatitude:(float)Latitude andLongitude:(float)Longitude andTime:(NSDate*)Time andAPIUrl:(NSString*)urlString
{
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    
    NSString* outString = [NSString stringWithFormat:@"ID=%@&Latitude=%f&Longitude=%f&Time=%@",LocationID,Latitude,Longitude,Time];
    NSData *requestData = [outString dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody: requestData];
    
    [[NSURLConnection alloc] initWithRequest:request delegate:self];
}

#pragma - NSURL Delegate

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSLog(@"Resp received");
}

- (void)connection:(NSURLConnection *)connection
    didReceiveData:(NSData *)data
{
    NSLog(@"Data received");
}

- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
    NSLog(@"ERROR: Achtung !: %@",[error localizedDescription]);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH , 0), ^{
        NSLog(@"FinishedLoading: In bg thread, do something with data here");
        
        dispatch_async( dispatch_get_main_queue(), ^{
            NSLog(@"FinishedLoading: In Main thread, access the UI here");
        });
    });
}

@end