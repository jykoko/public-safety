//
//  UIViewController+OtherAlertsViewController.m
//  PublicSafety
//
//  Created by Jacob Koko on 11/2/14.
//  Copyright (c) 2014 we.engineer.net.jykoko. All rights reserved.
//
#import "OtherAlertsViewController.h"
#import "PSImageCell.h"
#import "PSImageCaptureMediaHandler.h"
#import "PSLocManagerSingleton.h"

@implementation OtherAlertsViewController

@synthesize distTraveledLabel;

PSImageCaptureMediaHandler* imgHandler;
NSTimer *picTimer;
NSTimer *distTimer;

- (void)viewDidLoad
{
    [super viewDidLoad];
    //self.navigationController.navigationBar.tintColor = [UIColor redColor];
    //self.navigationItem.title = @"Other Alerts";
    
    //Adds a backbutton
    /*
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style: UIBarButtonItemStyleBordered target:self action:@selector(Back)];
    //Adds the custom red color to the back button
    [backButton setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor redColor]} forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = backButton;
     */
    
    /*
    UIImageView* bgImgViewForBlur =[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    bgImgViewForBlur.image = [UIImage imageNamed:@"IMG_NAME_HERE.PNG"];
     */
    
    self.imagesCollection.delegate = self;
    self.imagesCollection.dataSource = self;
    self.imagesCollection.allowsSelection = YES;
    
    imgHandler = [PSImageCaptureMediaHandler sharedSingleton];
    
    [self addBlurToView:self.view];
    
    self.distTraveledLabel.text = [NSString stringWithFormat:@"%.03f miles",[PSLocManagerSingleton sharedSingleton].distanceTraveled];
    picTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(showPic) userInfo:nil repeats:YES];
    distTimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(updateDistanceLabel) userInfo:nil repeats:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addBlurToView:(UIView *)view {
    UIView *blurView = nil;
    
    if([UIBlurEffect class]) { // iOS 8
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
        blurView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        blurView.frame = view.frame;
        
    } else { // workaround for iOS 7
        blurView = [[UIToolbar alloc] initWithFrame:view.bounds];
    }
    
    UIImageView* bgImg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    bgImg.image = [UIImage imageNamed:@"bg_imgview.png"];
    [view insertSubview:bgImg atIndex:0];
    
    [blurView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [view insertSubview:blurView aboveSubview:bgImg];
}

#pragma mark - UICollectionView data source

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if([imgHandler.walkLiveImages count] == 0)
    {
        return 1;
    }
    else
    {
        return [imgHandler.walkLiveImages count];
    }
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"image_cell";
    PSImageCell* cell = (PSImageCell*)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.parseImage.layer.cornerRadius =  cell.parseImage.frame.size.height/2;
    cell.parseImage.layer.masksToBounds = YES;
    //cell.parseImage.layer.borderWidth = 1;
    //[cell.parseImage.layer setBorderColor: [[UIColor blackColor] CGColor]];
    [cell.parseImage setHidden:YES];
    
    if ([imgHandler.walkLiveImages count] > 0)
    {
        [cell.parseImage setHidden:NO];
        cell.parseImage.image = [imgHandler.walkLiveImages objectAtIndex:indexPath.row];
        NSLog(@"setting image cell %@",[imgHandler.walkLiveImages objectAtIndex:indexPath.row]);
        [cell.loadingSpinner stopAnimating];
    }
    else
    {
        [cell.parseImage setHidden:NO];
        cell.parseImage.image = [UIImage imageNamed:@"imgres.jpg"];
        [cell.loadingSpinner stopAnimating];
    }
    
    NSLog(@"setting image cell");
    
    return cell;
}

/**
 * Fix this, not being called!!!
 */
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    [cell setHighlighted:YES];
    [cell setSelected:YES];
    
    if([self.imagesArray count] != 0)
    {
        //[self enlargeThePhoto:nil];
    }
}

-(void)updateDistanceLabel
{
     self.distTraveledLabel.text = [NSString stringWithFormat:@"%.03f miles",[PSLocManagerSingleton sharedSingleton].distanceTraveled];
}

-(void)showPic
{
    if(imgHandler.liveCameraSnapShot != nil && [imgHandler.walkLiveImages count] > 0)
    {
        [self.imagesCollection reloadData];
        NSLog(@"reloading data...");
    }
}

- (IBAction)hideViewController:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end