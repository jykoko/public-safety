//
//  PSImageCaptureMediaHandler.h
//  PublicSafety
//
//  Created by Jacob Koko on 11/22/14.
//  Copyright (c) 2014 we.engineer.net.jykoko. All rights reserved.
//

#ifndef PublicSafety_PSImageCaptureMediaHandler_h
#define PublicSafety_PSImageCaptureMediaHandler_h

#import <UIKit/UIKit.h>


@interface PSImageCaptureMediaHandler : NSObject

@property UIImage* liveCameraSnapShot;
@property NSMutableArray* walkLiveImages;
@property NSMutableArray* walkLiveImageDataArray;
@property long reportID;

+ (PSImageCaptureMediaHandler*)sharedSingleton;
-(void)clearWalkImageDataArray;
-(void)clearWalkImages;

@end


#endif
