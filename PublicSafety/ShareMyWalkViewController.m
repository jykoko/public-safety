//
//  UIViewController+ShareMyWalkViewController.m
//  PublicSafety
//
//  Created by Jacob Koko on 11/2/14.
//  Copyright (c) 2014 we.engineer.net.jykoko. All rights reserved.
//

#import "ShareMyWalkViewController.h"
#import "PSRequestHandler.h"
#import "PSImageCaptureMediaHandler.h"
#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
const int SHARE_WALK_ID = 9;

@implementation ShareMyWalkViewController
@synthesize mapView = _mapView,routeLine,routeLineView,trackPointArray,routeRect,locationManager,userLocations,shareWalkBtn,progHud,sharedWalkSegControl;

BOOL isSharing;
int reportId;
UIBarButtonItem *shareButton;
NSDate* startTime;
//PSImageCaptureMediaHandler* imgHandler;

- (void)viewDidLoad
{
    [super viewDidLoad];
    _mapView.delegate = self;
    _mapView.showsUserLocation=YES;
    reportId = -1;
    
    self.navigationItem.title = @"Share Walk";
    
    //Adds a backbutton
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style: UIBarButtonItemStyleBordered target:self action:@selector(Back)];
    //Adds the custom red color to the back button
    [backButton setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor redColor]} forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = backButton;
    
    self.locationManager = [CLLocationManager new];
    [self.locationManager setDelegate:self];
    [self.locationManager setDistanceFilter:kCLDistanceFilterNone];
    [self.locationManager setHeadingFilter:kCLHeadingFilterNone];
    
    if(IS_OS_8_OR_LATER)
    {
        [self.locationManager requestAlwaysAuthorization];
    }
    
    [self.locationManager startUpdatingLocation];
    
    /*
    shareButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(sendShareWalkAlert)];
    self.navigationItem.rightBarButtonItem = shareButton;
     */
    
    isSharing = NO;
    //shareButton.enabled = NO;
    self.userLocations = [[NSMutableArray alloc]init];
    
    CGRect newBounds = CGRectMake(-5, 64, self.view.bounds.size.width+10, 55);
    self.sharedWalkSegControl = [[UISegmentedControl alloc]initWithFrame:newBounds];
    self.sharedWalkSegControl.backgroundColor = [UIColor whiteColor];
    self.sharedWalkSegControl.tintColor = [UIColor redColor];
    [self.sharedWalkSegControl insertSegmentWithTitle:@"Send Alert" atIndex:0 animated:YES];
    [self.sharedWalkSegControl insertSegmentWithTitle:@"Arrived Safely" atIndex:1 animated:YES];
    [self.mapView addSubview:sharedWalkSegControl];
    
    //attach target action for if the selection is changed by the user
    [self.sharedWalkSegControl addTarget:self
                                action:@selector(shareWalkPicker:)
                      forControlEvents:UIControlEventValueChanged];
    
    //startTime = [NSDate date];
    
    [self.sharedWalkSegControl setSelectedSegmentIndex:UISegmentedControlNoSegment];
    
    //imgHandler = [PSImageCaptureMediaHandler sharedSingleton];
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    MKCoordinateRegion mapRegion;
    mapRegion.center = mapView.userLocation.coordinate;
    mapRegion.span.latitudeDelta = 0.001;
    mapRegion.span.longitudeDelta = 0.001;
    [mapView setRegion:mapRegion animated: YES];
}


- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    if(isSharing)
    {
        [self.userLocations addObject:newLocation];
        
        MKMapPoint * pointsArray = malloc(sizeof(CLLocationCoordinate2D)*2);
        pointsArray[0]= MKMapPointForCoordinate(oldLocation.coordinate);
        pointsArray[1]= MKMapPointForCoordinate(newLocation.coordinate);
        
        routeLine = [MKPolyline polylineWithPoints:pointsArray count:2];
        free(pointsArray);
        NSLog(@"dist between point a and b is : %i",(int)newLocation.coordinate.latitude - (int)oldLocation.coordinate.latitude);
        
        if (newLocation.coordinate.latitude - oldLocation.coordinate.latitude < 1)
        {
            [self.mapView addOverlay:routeLine];
        }
        
        if(reportId != -1)
        {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSString* dateString = [dateFormatter stringFromDate:[NSDate date]];

            [PSRequestHandler addNewShareWalkReport:newLocation.coordinate.latitude andLongitude:newLocation.coordinate.longitude andDateString:dateString andReportId:reportId];
            /*
            if(imgHandler.liveCameraSnapShot)
            {
                NSData *picData = UIImagePNGRepresentation(imgHandler.liveCameraSnapShot);
                [PSRequestHandler addNewPictureToReport:picData andReportId:reportId andCompletion:^(NSDictionary *responseDict) {
                    if(responseDict)
                    {
                        NSLog(@"shared walk img success.");
                    }
                }];
            }
            */
        }
    }
}

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay
{
    MKOverlayView* overlayView = nil;
    self.routeLineView = [[MKPolylineView alloc] initWithPolyline:[self routeLine]];
    [[self routeLineView] setFillColor:[UIColor colorWithRed:255/255.0f green:0/255.0f blue:0/255.0f alpha:1.0]];
    [[self routeLineView] setStrokeColor:[UIColor colorWithRed:255/255.0f green:0/255.0f blue:0/255.0f alpha:1.0]];
    [[self routeLineView] setLineWidth:15.0];
    [[self routeLineView] setLineCap:kCGLineCapRound];
    overlayView = [self routeLineView];
    return overlayView;
}

- (IBAction)shareWalk:(id)sender
{
    [self.sharedWalkSegControl setSelectedSegmentIndex:UISegmentedControlNoSegment];
    
     //clear previous locations if they exist
    if([self.userLocations count] > 0)
    {
        [self.userLocations removeAllObjects]; //clear user locs when report sent
    }
    
    if(!isSharing)
    {
        isSharing = YES;
        shareWalkBtn.title = @"Sharing Walk...";
        shareButton.enabled = YES;
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString* dateString = [dateFormatter stringFromDate:[NSDate date]];
        NSNumber* alertID=[NSNumber numberWithLong:SHARE_WALK_ID];

        [PSRequestHandler addNewReport:dateString andLocations:self.userLocations andAlertID:alertID andCompletion:^(NSDictionary *responseDictionary) {
            if(responseDictionary)
            {
                NSString* lastIDInserted = [responseDictionary objectForKey:@"ID"];
                reportId = [lastIDInserted intValue];
                NSLog(@"Walk Report Id -> %i",reportId);
            }
        }];
    }
    else
    {
        isSharing = NO;
        shareWalkBtn.title = @"Share Walk";
        shareButton.enabled = NO;
    }
}

- (void) shareWalkPicker:(UISegmentedControl *)paramSender
{
    
    //check if its the same control that triggered the change event
    if ([paramSender isEqual:self.sharedWalkSegControl])
    {
        //get index position for the selected control
        NSInteger selectedIndex = [paramSender selectedSegmentIndex];
        
        NSString *myChoice =
        [paramSender titleForSegmentAtIndex:selectedIndex];
        
        if((int)selectedIndex == 0)
        {
            [self sendShareWalkAlert];
        }
        else if((int)selectedIndex == 1)
        {
            UIAlertView* cancelWalk = [[UIAlertView alloc]initWithTitle:@"Cancel Walk?" message:@"Are you sure you want to stop reporting your walk?" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
            [cancelWalk show];
        }
        
        NSLog(@"Segment at position %@ with %@ text is selected",
              [NSNumber numberWithInt:(int)selectedIndex], myChoice);
    }
}

- (IBAction)sendShareWalkAlert
{
    [self.sharedWalkSegControl setSelectedSegmentIndex:UISegmentedControlNoSegment];
    isSharing = NO;
    shareWalkBtn.title = @"Share Walk";
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            shareButton.enabled = NO;
            
            UIAlertView* success = [[UIAlertView alloc]initWithTitle:@"Success!" message:@"Your walk has been shared and help will be on the way shortly." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            
            [success show];
        });
    });
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        [self.sharedWalkSegControl setSelectedSegmentIndex:UISegmentedControlNoSegment];
        isSharing = NO;
        shareWalkBtn.title = @"Share Walk";
        shareButton.enabled = NO;
    }
    else
    {
         [self.sharedWalkSegControl setSelectedSegmentIndex:UISegmentedControlNoSegment];
    }
}


- (IBAction)Back
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end