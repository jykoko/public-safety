//
//  UICollectionViewCell+ImageCell.h
//  PublicSafety
//
//  Created by Jacob Koko on 11/22/14.
//  Copyright (c) 2014 we.engineer.net.jykoko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface  PSImageCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *parseImage;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingSpinner;

@end
