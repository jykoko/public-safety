//
//  UIViewController+SettingsViewController.h
//  PublicSafety
//
//  Created by Christian Decker on 11/16/14.
//  Copyright (c) 2014 we.engineer.net.jykoko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController <UINavigationControllerDelegate, UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *userLabel;
@property (weak, nonatomic) IBOutlet UIButton *userChangeSettings;
@property (weak, nonatomic) IBOutlet UITextField *userPinTexBox;

-(IBAction)setUserSettings:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *displayTutorial;
- (IBAction)displayTutorialVc:(id)sender;

@end
