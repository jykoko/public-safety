//
//  NSString+NSStringValidator.m
//  PublicSafety
//
//  Created by Antonio Garcia on 11/16/14.
//  Copyright (c) 2014 we.engineer.net.jykoko. All rights reserved.
//

#import "NSString+NSStringValidator.h"

@implementation NSString (NSStringValidator)


+ (BOOL)validateEmail:(NSString *)emailStr
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailStr];
}


+ (BOOL)validatePassword:(NSString *)passwordStr
{
    if ( [passwordStr length]<8 || [passwordStr length]>100 ) return NO;  // too long or too short
    NSRange rang;
    rang = [passwordStr rangeOfCharacterFromSet:[NSCharacterSet letterCharacterSet]];
    if ( !rang.length ) return NO;  // no letter
    rang = [passwordStr rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]];
    if ( !rang.length )  return NO;  // no number;
    return YES;
}

+ (BOOL)passwordsMatch:(NSString *)passwordStr andpasswordStrAgain:(NSString *)passwordStrAgain
{
    if ([passwordStr isEqualToString:passwordStrAgain])
    {
        return YES;
    }
    return NO;
}

@end
