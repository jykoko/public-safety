//
//  NSString+MD5.h
//  PublicSafety
//
//  Created by Antonio Garcia on 11/20/14.
//  Copyright (c) 2014 we.engineer.net.jykoko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MD5)

- (NSString *)MD5String;

@end
