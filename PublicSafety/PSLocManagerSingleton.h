//
//  LocationManagerSingleton.h
//  SkateFinder
//
//  Created by Jacob Koko on 8/6/14.
//
//
#import <MapKit/MapKit.h>

@interface PSLocManagerSingleton : NSObject <CLLocationManagerDelegate>

@property (nonatomic, strong) NSMutableArray* userLocations;
@property (nonatomic, strong) CLLocationManager* locationManager;
@property CLLocation* startingLoc;
@property float distanceTraveled;

+ (PSLocManagerSingleton*)sharedSingleton;
- (void) clearUserLocations;

@end