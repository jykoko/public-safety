//
//  ViewController.m
//  PublicSafety
//
//  Created by Jacob Koko on 10/15/14.
//  Copyright (c) 2014 we.engineer.net.jykoko. All rights reserved.
//

#import "ViewController.h"
#import "MainViewController.h"
#import "SWRevealViewController.h"
#import "PSRequestHandler.h"
#import "MBProgressHUD.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize userNameTextField,passWordTextField;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.userNameTextField.delegate = self;
    self.passWordTextField.delegate = self;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    if([defaults boolForKey:@"LoggedIn"] == YES)
    {
        [self openMainViewController];
    }
    else
    {
        NSLog(@"no");
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)login:(id)sender
{
    MBProgressHUD* HUD =[[MBProgressHUD alloc]init];
    HUD.labelText = @"Authenticating...";
    [HUD show:YES];
    [self.view addSubview:HUD];

    if(self.userNameTextField.text.length == 0)
    {
        [HUD hide:YES];
        UIAlertView* errorMessage = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Please complete all fields before logging in!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [errorMessage show];
        return;
    }
    
    if(self.passWordTextField.text.length == 0)
    {
        [HUD hide:YES];
        UIAlertView* errorMessage = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Please complete all fields before logging in!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [errorMessage show];
        return;
    }
    
    [PSRequestHandler attemptLoginWithUserName:self.userNameTextField.text andPassWord:self.passWordTextField.text andCompletion:^(NSDictionary *jsonResponse) {
        if(jsonResponse)
        {
            //store user info to phone to prevent future network queries for user data
            NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:jsonResponse];
            [defaults setObject:data forKey:@"userDetails"];
            [defaults setBool:YES forKey:@"LoggedIn"];
            [defaults synchronize];
            
            [HUD hide:YES];
            
            //valid user, so continue
            [self openMainViewController];
        }
        else
        {
            [HUD hide:YES];
            
            UIAlertView* errorMessage = [[UIAlertView alloc]initWithTitle:@"Invalid User" message:@"This user was not found, please try again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [errorMessage show];
        }
    }];
}

-(void)openMainViewController
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    SWRevealViewController *mainVc = [storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
    [self presentViewController:mainVc animated:YES completion:nil];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

@end