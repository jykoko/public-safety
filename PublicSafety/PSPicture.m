//
//  PSPicture.m
//  PublicSafety
//
//  Created by Antonio Garcia on 10/26/14.
//  Copyright (c) 2014 we.engineer.net.jykoko. All rights reserved.
//

#import "PSPicture.h"
#import "AFNetworking.h"
#import "PSConstants.h"

@interface PSPicture()
//private instances go here...
@end

@implementation PSPicture

@synthesize pictureId = _pictureId;
@synthesize pictureImagePath = _pictureImagePath;

/*
 * Get particular picture based on pictureId from request handler
 * I created completion block handlers to copy Parse because we are familiar with Parse
 */
-(void)getPicture:(NSNumber*)pictureId
{
    /*
    [PSRequestHandler getPictureFromJSON:pictureId andUrlString:PSPictureApiBaseUrl andCompletion:^(NSDictionary *jsonResponse) {
        if(jsonResponse != nil)
        {
            self.pictureId=jsonResponse[@"ID"];
            self.pictureImagePath=jsonResponse[@"Path"];
            NSLog(@"picture ID: %@", self.pictureId);
            NSLog(@"picture path: %@", self.pictureImagePath);
        }
    }];
     */
}

/*
 * Get list of all Pictures from request handler
 * I created completion block handlers to copy Parse because we are familiar with Parse
 */
-(void)getAllPictures
{
    [PSRequestHandler getAllPicturesFromJSON:PSSchoolApiBaseUrl andCompletion:^(NSDictionary *jsonResponse) {
        if(jsonResponse != nil)
        {
            self.pictureId=jsonResponse[@"ID"];
            self.pictureImagePath=jsonResponse[@"Path"];
            NSLog(@"picture ID: %@", self.pictureId);
            NSLog(@"picture path: %@", self.pictureImagePath);
        }
    }];
}


@end
