//
//  PSCountDownAlertView.m
//  PublicSafety
//
//  Created by Jacob Koko on 11/14/14.
//  Copyright (c) 2014 we.engineer.net.jykoko. All rights reserved.
//
#import "PSCountDownAlertView.h"

@interface PSCountDownAlertView()
{
    UILabel *progress;
    NSTimer *timer;
    int currSeconds;
    BOOL alertTimedOut;
}
@end


@implementation PSCountDownAlertView

@synthesize isShown;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        alertTimedOut = NO;
        originalFrame = frame;
        
        self.alpha = 0;
        self.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.7f];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, 20)];
        label.text = @"Cancel Report?";
        label.textAlignment = UITextAlignmentCenter;
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor whiteColor];
        [self addSubview:label];
        
        progress=[[UILabel alloc] initWithFrame:CGRectMake(10, 30, 300, 100)];
        progress.textColor=[UIColor whiteColor];
        [progress setText:@"15 s"];
        progress.textAlignment = UITextAlignmentCenter;
        progress.font=[progress.font fontWithSize:75];
        progress.backgroundColor=[UIColor clearColor];
        [self addSubview:progress];
        currSeconds=15;
        [self start];
        
        UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        closeButton.frame = CGRectMake(20, frame.size.height - 45, frame.size.width-40, 35);
        [closeButton setTitle:@"Cancel Report" forState:UIControlStateNormal];
        [closeButton addTarget:self action:@selector(hide) forControlEvents:UIControlEventTouchUpInside];
        closeButton.backgroundColor = [UIColor whiteColor];
        [closeButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [self addSubview:closeButton];
        
    }
    return self;
}


#pragma mark Custom alert methods

//show custom alert view with animation
- (void)show
{
    NSLog(@"show");
    isShown = YES;
    self.transform = CGAffineTransformMakeScale(0.1, 0.1);
    self.alpha = 0;
    [UIView beginAnimations:@"showAlert" context:nil];
    [UIView setAnimationDelegate:self];
    self.transform = CGAffineTransformMakeScale(1.1, 1.1);
    self.alpha = 1;
    [UIView commitAnimations];
}

//hide custom alert view with animation
- (void)hide
{
    isShown = NO;
    [UIView beginAnimations:@"hideAlert" context:nil];
    [UIView setAnimationDelegate:self];
    self.transform = CGAffineTransformMakeScale(0.1, 0.1);
    self.alpha = 0;
    [UIView commitAnimations];
    
    if(!alertTimedOut)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Restricted"
                                                        message:@"Please Enter 4 Digit Code to Cancel Report"
                                                       delegate:self
                                              cancelButtonTitle:@"Exit"
                                              otherButtonTitles:@"Submit"
                              , nil];
        
        [[alert textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeNumberPad];
        [[alert textFieldAtIndex:0] becomeFirstResponder];
        alert.alertViewStyle = UIAlertViewStyleSecureTextInput;
        [alert show];
    }
}


//toggle custom alert view display state
- (void)toggle
{
    if (isShown)
    {
        [self hide];
    }
    else
    {
        [self show];
    }
}

#pragma mark Animation delegate

//create custom animation for alert view show/hide
- (void)animationDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    if ([animationID isEqualToString:@"showAlert"])
    {
        if (finished)
        {
            [UIView beginAnimations:nil context:nil];
            self.transform = CGAffineTransformMakeScale(1.0, 1.0);
            [UIView commitAnimations];
        }
    }
    else if ([animationID isEqualToString:@"hideAlert"])
    {
        if (finished)
        {
            self.transform = CGAffineTransformMakeScale(1.0, 1.0);
            self.frame = originalFrame;
        }
    }
}

#pragma mark Touch methods

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    lastTouchLocation = [touch locationInView:self];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint newTouchLocation = [touch locationInView:self];
    CGRect currentFrame = self.frame;
    
    CGFloat deltaX = lastTouchLocation.x - newTouchLocation.x;
    CGFloat deltaY = lastTouchLocation.y - newTouchLocation.y;
    
    self.frame = CGRectMake(currentFrame.origin.x - deltaX, currentFrame.origin.y - deltaY, currentFrame.size.width, currentFrame.size.height);
    lastTouchLocation = [touch locationInView:self];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

#pragma timer methods

//called to start count down timer using a simple NSTimer object
-(void)start
{
    timer=[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerFired) userInfo:nil repeats:YES];
    
}

//called every second by the scheduled timer above, it decrements the 30 sec counter
-(void)timerFired
{
    if((currSeconds>=0))
    {
        if(currSeconds==0)
        {
            alertTimedOut = YES;
            [self hide];
            [timer invalidate];
        }
        else if(currSeconds>0)
        {
            currSeconds-=1;
        }
        
        [progress setText:[NSString stringWithFormat:@"%i s",currSeconds]];
    }
    else
    {
        [timer invalidate];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        NSLog(@"user canceled cancellation!");
    }
    else
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if(defaults)
        {
            NSString *passcode = [defaults objectForKey:@"securityPin"];
            NSString *enteredPin = [alertView textFieldAtIndex:0].text;
            if([passcode isEqualToString:enteredPin])
            {
                UIAlertView* success = [[UIAlertView alloc]initWithTitle:@"Success" message:@"The report has been canceled!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [success show];
            }
            else
            {
                UIAlertView* error = [[UIAlertView alloc]initWithTitle:@"Failed" message:@"The report has not been canceled, try-again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [error show];
            }
        }

    }
}

@end