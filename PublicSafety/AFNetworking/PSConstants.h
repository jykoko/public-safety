//
//  NSObject+PSConstants.h
//  PublicSafety
//
//  Created by Jacob Koko on 10/26/14.
//  Copyright (c) 2014 we.engineer.net.jykoko. All rights reserved.
//

#import <Foundation/Foundation.h>

//API Constants
static NSString* const BaseURLString = @"http://caffeinatedcm-001-site3.smarterasp.net/";
static NSString * const PSUserApiBaseUrl = @"http://caffeinatedcm-001-site3.smarterasp.net/api/v2/User";
static NSString * const PSAlertApiBaseUrl = @"http://caffeinatedcm-001-site3.smarterasp.net/api/v1/Alert";
static NSString* const PSReportApiBaseUrl = @"http://caffeinatedcm-001-site3.smarterasp.net/api/v1/Report";
static NSString* const PSLocationApiBaseUrl = @"http://caffeinatedcm-001-site3.smarterasp.net/api/v1/Location";
static NSString* const PSPictureApiBaseUrl = @"http://caffeinatedcm-001-site3.smarterasp.net/api/v1/Picture";
static NSString* const PSSchoolApiBaseUrl = @"http://caffeinatedcm-001-site3.smarterasp.net/api/v1/School";
static NSString* const PSLoginApiBaseUrl = @"http://caffeinatedcm-001-site3.smarterasp.net/api/v2/Login";


