/*
 * Sidebar used for slide out menu bar
 */
#import "SidebarViewController.h"
#import "SWRevealViewController.h"
#import "UserProfileViewController.h"
#import "SettingsViewController.h"

@interface SidebarViewController ()

@property (nonatomic, strong) NSArray *menuItems;
@end

@implementation SidebarViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithWhite:0.2f alpha:1.0f];
    self.tableView.backgroundColor = [UIColor colorWithWhite:0.2f alpha:1.0f];
    self.tableView.separatorColor = [UIColor colorWithWhite:0.15f alpha:0.2f];
    
    _menuItems = @[@"Settings",@"Profile"];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.menuItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    //set cell title
    cell.textLabel.text = [self.menuItems objectAtIndex:indexPath.row];
    
    //set cell image based on title
    NSString *cellText = cell.textLabel.text.lowercaseString;
    if([cellText isEqualToString:@"profile"])
    {
        cell.imageView.image = [UIImage imageNamed:@"usericon.png"];
    }
    else if([cellText isEqualToString:@"settings"])
    {
        cell.imageView.image = [UIImage imageNamed:@"settingsicon.png"];
    }
        
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    NSString *cellText = cell.textLabel.text.lowercaseString;
    if([cellText isEqualToString:@"profile"])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        UserProfileViewController *profileVc = [storyboard instantiateViewControllerWithIdentifier:@"UserProfileViewController"];
        UINavigationController *navigationController =
        [[UINavigationController alloc] initWithRootViewController:profileVc];
        
        //now present this navigation controller modally
        [self presentViewController:navigationController
                           animated:YES
                         completion:^{
                             
                         }];
    }
    else if([cellText isEqualToString:@"settings"])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        SettingsViewController *SettingsViewController = [storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
        UINavigationController *navigationController =
        [[UINavigationController alloc] initWithRootViewController:SettingsViewController];
        
        //now present this navigation controller modally
        [self presentViewController:navigationController
                           animated:YES
                         completion:^{
                             
                         }];

    }
}

@end