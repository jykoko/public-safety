//
//  PSCountDownAlertView.h
//  PublicSafety
//
//  Created by Jacob Koko on 11/14/14.
//  Copyright (c) 2014 we.engineer.net.jykoko. All rights reserved.
//
#import <UIKit/UIKit.h>

@interface  PSCountDownAlertView : UIView <UIAlertViewDelegate>
{
    CGPoint lastTouchLocation;
    CGRect originalFrame;
    BOOL isShown;
}

//this value keeps tack of custom alert view state
@property (nonatomic) BOOL isShown;

//basic methods to hide/show custom alert view
- (void)show;
- (void)hide;

@end
