//
//  PSImageCaptureMediaHandler.m
//  PublicSafety
//
//  Created by Jacob Koko on 11/22/14.
//  Copyright (c) 2014 we.engineer.net.jykoko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PSImageCaptureMediaHandler.h"
#import <AVFoundation/AVFoundation.h>
#import "PSRequestHandler.h"

@interface PSImageCaptureMediaHandler()
@end

@implementation PSImageCaptureMediaHandler
@synthesize liveCameraSnapShot,walkLiveImages,reportID,walkLiveImageDataArray;

AVCaptureSession* session;
NSTimer *timer;
AVCaptureConnection *videoConnection;
AVCaptureStillImageOutput *output;

+ (PSImageCaptureMediaHandler*)sharedSingleton
{
    static PSImageCaptureMediaHandler* sharedSingleton;
    if(!sharedSingleton)
    {
        @synchronized(sharedSingleton)
        {
            sharedSingleton = [PSImageCaptureMediaHandler new];
        }
    }
    
    return sharedSingleton;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        self.walkLiveImages = [[NSMutableArray alloc]init];
        self.walkLiveImageDataArray = [[NSMutableArray alloc]init];
        self.liveCameraSnapShot = [[UIImage alloc]init];
        // Get all cameras in the application and find the frontal camera.
        AVCaptureDevice *frontCamera;
        NSArray *allCameras = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
        
        // Find the frontal camera.
        for ( int i = 0; i < allCameras.count; i++ ) {
            AVCaptureDevice *camera = [allCameras objectAtIndex:i];
            
            if ( camera.position == AVCaptureDevicePositionBack ) {
                frontCamera = camera;
            }
        }
        
        // If we did not find the camera then do not take picture.
        if( frontCamera != nil )
        {
            // Start the process of getting a picture.
            session = [[AVCaptureSession alloc] init];
            
            // Setup instance of input with frontal camera and add to session.
            NSError *error;
            AVCaptureDeviceInput *input =
            [AVCaptureDeviceInput deviceInputWithDevice:frontCamera error:&error];
            
            if(!error && [session canAddInput:input] )
            {
                // Add frontal camera to this session.
                [session addInput:input];
                
                // We need to capture still image.
                output = [[AVCaptureStillImageOutput alloc] init];
                
                // Captured image. settings.
                [output setOutputSettings:
                [[NSDictionary alloc] initWithObjectsAndKeys:AVVideoCodecJPEG,AVVideoCodecKey,nil]];
                
                if( [session canAddOutput:output] )
                {
                    [session addOutput:output];
                    
                    videoConnection = nil;
                    for (AVCaptureConnection *connection in output.connections)
                    {
                        for (AVCaptureInputPort *port in [connection inputPorts])
                        {
                            if ([[port mediaType] isEqual:AVMediaTypeVideo] )
                            {
                                videoConnection = connection;
                                break;
                            }
                        }
                       
                        if (videoConnection)
                        {
                            break;
                        }
                    }
                    
                    [self capturePhoto];
                }
            }
        }
    }
    
    return self;

}

//called to start taking pics repeatedly
-(void)capturePhoto
{
    timer=[NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(timerFired) userInfo:nil repeats:YES];
}


//called every second by the scheduled timer above to take pic
-(void)timerFired
{
    // Finally take the picture
    if( videoConnection )
    {
        [session startRunning];
        [output captureStillImageAsynchronouslyFromConnection:videoConnection completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error) {
            
            if(imageDataSampleBuffer != NULL)
            {
                NSData *imageData = [AVCaptureStillImageOutput
                                     jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
                UIImage* unscaledImage = [UIImage imageWithData:imageData];
                
                UIImage* scaledAndOrientedImage = [self scaleImage:unscaledImage toSize:CGSizeMake(300, 300)];
                NSData *imageDataCompressed = UIImageJPEGRepresentation(scaledAndOrientedImage,0.8);
                self.liveCameraSnapShot = [UIImage imageWithData:imageDataCompressed];
                
                if(self.liveCameraSnapShot != nil)
                {
                    [self.walkLiveImageDataArray addObject:imageDataCompressed];
                    [walkLiveImages addObject:liveCameraSnapShot];
                }
                
                NSLog(@"image captured");
                NSLog(@"data image count : %lu", [self.walkLiveImageDataArray count]);
                NSLog(@"image count : %lu", [self.walkLiveImages count]);
            }
        }];
    }
}

-(void)clearWalkImages
{
    [self.walkLiveImages removeAllObjects];
}


-(void)clearWalkImageDataArray
{
    [self.walkLiveImageDataArray removeAllObjects];
}

/*
 * Put these methods in seperate photo utility class!
 */
- (UIImage*)scaleImage:(UIImage*)image toSize:(CGSize)newSize {
    CGSize scaledSize = newSize;
    float scaleFactor = 1.0;
    if( image.size.width > image.size.height ) {
        scaleFactor = image.size.width / image.size.height;
        scaledSize.width = newSize.width;
        scaledSize.height = newSize.height / scaleFactor;
    }
    else {
        scaleFactor = image.size.height / image.size.width;
        scaledSize.height = newSize.height;
        scaledSize.width = newSize.width / scaleFactor;
    }
    
    UIGraphicsBeginImageContextWithOptions( scaledSize, NO, 0.0 );
    CGRect scaledImageRect = CGRectMake( 0.0, 0.0, scaledSize.width, scaledSize.height );
    [image drawInRect:scaledImageRect];
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return scaledImage;
}


@end