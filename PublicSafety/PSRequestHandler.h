/*
 * This class is a wrapper around AFNetworking, and it stores ALL of the GET/POST/PUT
 * functions needed for the app. We use asychronous calls on background threads to
 * prevent slow load times. The methods are designed to mock the Parse SDK that everyone
 * is already familiar with, so it uses completion blocks.
 */
#import <Foundation/Foundation.h>
#import "AFNetworking.h"

@interface PSRequestHandler : NSObject <NSURLConnectionDelegate>

@property __block int lastInsertId;

/*
 * Authentication API Requests
 */
+ (void)attemptLoginWithUserName:(NSString *)userName andPassWord:(NSString *)passWord andCompletion:(void (^)(NSDictionary* jsonResponse))completion;

/*
 * Alert API Requests
 */
+ (void)addNewAlert:(NSString*)alertId andSchoolID:(NSNumber*)schoolID andColor:(NSString*)color
   andEnabledStatus:(BOOL)enabledStatus
          andAPIUrl:(NSString*)urlString;
+ (void)getAlerts:(void(^)(NSDictionary *dictionary))completion;

/*
 * User API Requests
 */
/*
+ (void)addNewUser:(NSString*)fName andLastName:(NSString*)lName andUserName:(NSString*)userName
                                                               andPassWord:(NSString*)passWord
                                                                  andOrgId:(NSNumber*)organizationId
                                                           andProfilePicId:(NSNumber*)profilePicId
                                                                 andRoleId:(NSNumber*)roleId
                                                               andSchoolId:(NSNumber*)schoolID
                                                                 andUserId:(NSNumber*)userId
                                                                 andAPIUrl:(NSString*)urlString;
 */
+(void)addNewUser:(NSString*)userName andPassword:(NSString*)
passWord andRoleId:(NSNumber*)roleID andFName:(NSString*)fName andLName:(NSString*)lName
      andSchoolID:(NSNumber*)schoolID andPictureId:(NSNumber*)pictureID andOrgID:(NSString*)orgID andCompletion:(void (^)(NSDictionary* jsonResponse))completion;

+ (void)getAllUsersFromJSON:(NSString*)urlString andCompletion:(void (^)(NSDictionary* jsonResponse))completion;
+ (void)getUserFromJSON:(int)userID andCompletion:(void (^)(NSDictionary* jsonResponse))completion;


/*
 * Report API Requests
 */
+(void)addNewReport:(NSString*)dateString andLocations:(NSMutableArray*)locations andAlertID:(NSNumber*)AlertID andCompletion:(void(^)(NSDictionary *dictionary))completion;

/*
 * ShareWalk API Requests
 */
+(void)addNewShareWalkReport:(float)latitude andLongitude:(float)longitude andDateString:(NSString *)dateString andReportId:(int)reportId;

/*
 * School API Requests
 */
+ (void)addNewSchool:(NSNumber*)schoolId andSchoolEmail:(NSString*)schoolEmail
    andSchoolAddress:(NSString*)schoolAddress andSchoolPhoneNumber:(NSString*)schoolPhoneNumber;

+ (void)getAllSchoolsFromJSON:(NSString*)urlString andCompletion:(void (^)(NSDictionary* jsonResponse))completion;
+ (void)getSchoolFromJSON:(NSNumber*)schoolId andUrlString:(NSString*)urlString andCompletion:(void (^)(NSDictionary* jsonResponse))completion;

/*
 * Picture API Requests
 */
+ (void)addNewPicture:(NSData*)pictureData andCompletion:(void (^)(NSDictionary *responseDict))completion;

+ (void)addNewPictureToReport:(NSData*)pictureData andReportId:(int)reportID andCompletion:(void (^)(NSDictionary *responseDict))completion;

+ (void)getAllPicturesFromJSON:(NSString*)urlString andCompletion:(void (^)(NSDictionary* jsonResponse))completion;

+ (void)getPictureFromJSON:(int)pictureID andCompletion:(void (^)(NSDictionary* jsonResponse))completion;

/*
 * Location API Request
 */
+ (void)addNewLocation:(NSNumber*)LocationID andLatitude:(float)Latitude andLongitude:(float)Longitude andTime:(NSDate*)Time andAPIUrl:(NSString*)urlString;


@end