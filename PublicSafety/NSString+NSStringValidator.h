//
//  NSString+NSStringValidator.h
//  PublicSafety
//
//  Created by Antonio Garcia on 11/16/14.
//  Copyright (c) 2014 we.engineer.net.jykoko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (NSStringValidator) 
+ (BOOL)validateEmail:(NSString *)emailStr;
+ (BOOL)passwordsMatch:(NSString *)passwordStr andpasswordStrAgain:(NSString *)passwordStrAgain;
+ (BOOL)validatePassword:(NSString *)passwordStr;
@end
