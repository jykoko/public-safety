//
//  RegistrationStepOneViewController.h
//  PublicSafety
//
//  Created by Jacob Koko on 11/15/14.
//  Copyright (c) 2014 we.engineer.net.jykoko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface  RegistrationStepOneViewController : UIViewController <UITextFieldDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (strong, nonatomic) IBOutlet UIImageView *userProfileImageView;
@property (strong, nonatomic) IBOutlet UITextField *userEmailAddressField;
@property (strong, nonatomic) IBOutlet UITextField *userFullNameField;
@property (strong, nonatomic) IBOutlet UITextField *userUsernameField;
@property (strong, nonatomic) IBOutlet UITextField *userPassWordField;
@property (strong, nonatomic) IBOutlet UITextField *userPasswordAgainField;
@property (strong, nonatomic) IBOutlet UITextField *userCancellationPinField;
@property (strong, nonatomic) IBOutlet UIButton *clearAddPhotoBtn;
@property (weak, nonatomic) IBOutlet UIButton *registerBtn;
@property NSString *pin;

- (IBAction)uploadUserPhoto:(id)sender;

@end
